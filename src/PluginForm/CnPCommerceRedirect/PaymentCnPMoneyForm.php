<?php

namespace Drupal\clickandpledge_drupalcommerce\PluginForm\CnPCommerceRedirect;

use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CssCommand;

/**
 * The payment processing form.
 */
class PaymentCnPMoneyForm extends BasePaymentMethodAddForm {

  /**
   * {@inheritdoc}
   */
  public function buildCreditCardForm(array $element, FormStateInterface $form_state) {
    // Build a month select list that shows months with a leading zero.
    $textmonths = [
      1 => t('January'),
      2 => t('February'),
      3 => t('March'),
      4 => t('April'),
      5 => t('May'),
      6 => t('June'),
      7 => t('July'),
      8 => t('August'),
      9 => t('September'),
      10 => t('October'),
      11 => t('November'),
      12 => t('December'),
    ];
    $months = [];
    for ($i = 1; $i < 13; $i++) {
      $month = str_pad($i, 2, '0', STR_PAD_LEFT);
      $months[$month] = $textmonths[$i] . " (" . $month . ")";
    }
    // Build a year select list that uses a 4 digit key with a 2 digit value.
    $current_year_4 = date('Y');
    $current_year_2 = date('Y');
    $years = [];
    for ($i = 0; $i < 18; $i++) {
      $years[$current_year_4 + $i] = $current_year_2 + $i;
    }

    $config = \Drupal::config('cnp.mainsettings');
    $element['#attributes']['class'][] = 'credit-card-form';
    if ($config->get('cnp.cnp_status_enable')['yes'] === 'yes') {
      $element['cnp_fef_front_end_form_start'] = [
        '#prefix' => '<div class="cnp_fef_front_end">',
      ];

      $element['fef_some_text_start'] = [
        '#prefix' => '<div class="cnp_payment_heading"><p>' . t('Secured credit card payment with Click and Pledge API.') . '</p>',
        '#suffix' => '</div>',
      ];

      $cnplogo = "<img alt='Click&Pledge Secured' src='" . base_path() . drupal_get_path('module', 'clickandpledge_drupalcommerce') . "/images/logo.jpg'>";

      $element['cnp_logo'] = [
        '#prefix' => '<div class="cnp_logo">' . $cnplogo,
        '#suffix' => '</div>',
      ];
      $payOption = '';
      if (isset($_POST['payment_information']['add_payment_method']['payment_details']['fef_payment_options'])) {
        $payOption = $_POST['payment_information']['add_payment_method']['payment_details']['fef_payment_options'];
        if ($payOption == 'Recurring') {
          $element['cnppayoption'] = [
            '#type' => 'hidden',
            '#value' => $payOption,
            '#attributes' => ['id' => 'cnppayoption'],
          ];
        }
        else {
          $element['cnppayoption'] = [
            '#type' => 'hidden',
            '#value' => $payOption,
            '#attributes' => ['id' => 'cnppayoption'],
          ];
        }
      }
      else {
        $element['cnppayoption'] = [
          '#type' => 'hidden',
          '#value' => $payOption,
          '#attributes' => ['id' => 'cnppayoption'],
        ];
      }

      if ($config->get('cnp.cnp_recurr_oto')['oto'] === "oto" && $config->get('cnp.cnp_recurr_recur')[1] == 1) {
        $element['fef_recurring_payment_lbl'] = [
          '#prefix' => '<div class="cnp_form_recurring_label"><p><b>' . $config->get('cnp.cnp_recurr_label') . '</b></p>',
          '#suffix' => '</div>',
        ];
        $element['fef_payment_options_lbl'] = [
          '#prefix' => '<div class="cnp_form_recurr_settings"><p><b>' . $config->get('cnp.cnp_recurr_settings') . ' <span class="cnpstar_color">*</span></b></p>',
          '#suffix' => '</div>',
        ];
        $element['cnp_form_payment_options_start'] = [
          '#prefix' => '<div class="cnp_form_payment_options">',
        ];
        $element['fef_payment_options'] = [
          '#type' => 'radios',
          '#attributes' => ['data-payment-options' => 'fef_payment_options'],
          '#options' => ['One Time Only' => t('One Time Only'), 'Recurring' => t('Recurring')],
          '#prefix' => '<div class="container-inline">',
          '#suffix' => '</div>',
          '#default_value' => $config->get('cnp.cnp_default_payment_options'),
          '#ajax' => [
            'callback' => [$this, 'displayRecurringForm'],
          ],
        ];
        $element['cnp_form_payment_options_end'] = [
          '#suffix' => '</div>',
        ];
        // Toggle to display recurring options form.
        if ($config->get('cnp.cnp_default_payment_options') === 'Recurring') {
          $cssClass = 'recurring_div_show';
        }
        else {
          $cssClass = 'recurring_div_hide';
        }

        $element['fef_recurr_options_div_starts'] = [
          '#prefix' => '<div class="' . $cssClass . '" id="fef_recurr_options_division">',
        ];

        // Check recurring type checkbox values. if both are checked,display
        // dropdown with both options or else display individually.
        $rto = $config->get('cnp.cnp_recurr_type_option');

        if ($rto['Installment'] === 'Installment' && $rto['Subscription'] === 'Subscription') {
          $element['cnp_form_fef_recuring_type_option_start'] = [
            '#prefix' => '<div class="cnp_form_recuring_type_option">',
          ];

          $element['fef_recuring_type_option'] = [
            '#type' => 'select',
            '#title' => $config->get('cnp.cnp_recurring_types' . '<span style="color:red"> *</span>'),
            '#options' => $rto,
            '#default_value' => $config->get('cnp.cnp_default_recurring_type'),
          ];

          $element['cnp_form_fef_recuring_type_option_end'] = [
            '#suffix' => '</div>',
          ];
        }
        else {

          $othopt = $config->get('cnp.cnp_recurr_type_option');

          if ($othopt['Installment'] === 'Installment' && $othopt['Subscription'] === 0) {
            $element['fef_recurr_options_label_display'] = [
              '#prefix' => '<div class="cnp_form_recurr_options_label"><p>' . $config->get('cnp.cnp_recurring_types') . ': ' . $othopt['Installment'] . '</p>',
              '#suffix' => '</div>',
            ];
            $element['fef_recuring_type_option'] = [
              '#type' => 'hidden',
              '#value' => $othopt['Installment'],
            ];
          }
          elseif ($othopt['Installment'] === 0 && $othopt['Subscription'] === 'Subscription') {
            $element['fef_recurr_options_label_display'] = [
              '#prefix' => '<div class="cnp_form_recurr_options_label"><p>' . $config->get('cnp.cnp_recurring_types') . ': ' . $othopt['Subscription'] . '</p>',
              '#suffix' => '</div>',
            ];
            $element['fef_recuring_type_option'] = [
              '#type' => 'hidden',
              '#value' => $othopt['Subscription'],
            ];
          }
        }
        // DISPLAY PERIODCITY: IF IT IS ONE OPTION DISPLAY DIRECTLY OR MULTIPLE
        // OPTIONS DISPLAY ALL OPTIONS AS DROPDOWN.
        $periodcityOpt = $config->get('cnp.cnp_recurring_periodicity_options');
        $periodOpt = [];
        foreach ($periodcityOpt as $popts) {
          if ($popts !== 0) {
            $periodOpt[$popts] = $popts;
          }
        }
        if (count($periodOpt) == 1) {
          $element['fef_cnp_periodcity_label_display'] = [
            '#prefix' => '<div class="cnp_form_periodcity_label"><p>' . $config->get('cnp.cnp_recurring_periodicity') . ': ' . array_values($periodOpt)[0] . '</p>',
            '#suffix' => '</div>',
          ];
          $element['fef_cnp_periodcity'] = [
            '#type' => 'hidden',
            '#value' => array_values($periodOpt)[0],
          ];

        }
        else {
          $element['cnp_form_periodcity_start'] = [
            '#prefix' => '<div class="cnp_form_periodcity">',
          ];

          $element['fef_cnp_periodcity'] = [
            '#type' => 'select',
            '#title' => $config->get('cnp.cnp_recurring_periodicity' . '<span style="color:red"> *</span>'),
            '#options' => $periodOpt,
          ];

          $element['cnp_form_periodcity_end'] = [
            '#suffix' => '</div>',
          ];

        }
        // Display default no.of payments based on no.of payments radio buttons.
        $nop = $config->get('cnp.cnp_recurring_no_of_payments_options');
        if ($nop === 'fixednumber') {
          $config->get('cnp.cnp_recurring_default_no_payment_fncc_lbl');
          $element['fef_cnp_noof_payments_label_display'] = [
            '#prefix' => '<div class="cnp_form_noof_payments_label"><p>' . $config->get('cnp.cnp_recurring_no_of_payments') . ': ' . $config->get('cnp.cnp_recurring_default_no_payments_fnnc') . '</p>',
            '#suffix' => '</div>',
          ];
          $element['fef_cnp_noof_payments'] = [
            '#type' => 'hidden',
            '#default_value' => $config->get('cnp.cnp_recurring_default_no_payments_fnnc'),
          ];
        }
        elseif ($nop === 'indefinite_openfield') {
          $element['cnp_form_noof_payments_start'] = [
            '#prefix' => '<div class="cnp_form_noof_payments">',
          ];

          $element['fef_cnp_noof_payments'] = [
            '#type' => 'number',
            '#title' => $config->get('cnp.cnp_recurring_no_of_payments') . "<span class='redcolor'>*</span>",
            '#default_value' => $config->get('cnp.cnp_recurring_default_no_payments_open_filed'),
          ];

          $element['cnp_form_noof_payments_end'] = [
            '#suffix' => '</div>',
          ];

        }
        elseif ($nop === 'openfield') {
          $element['cnp_form_noof_payments_start'] = [
            '#prefix' => '<div class="cnp_form_noof_payments">',
          ];

          $element['fef_cnp_noof_payments'] = [
            '#type' => 'number',
            '#title' => $config->get('cnp.cnp_recurring_no_of_payments') . " <span class='redcolor'>*</span>",
            '#default_value' => $config->get('cnp.cnp_recurring_default_no_payments'),
          ];

          $element['cnp_form_noof_payments_end'] = [
            '#suffix' => '</div>',
          ];
        }
        else {
          $element['fef_cnp_noof_payments_label_display'] = [
            '#prefix' => '<div class="cnp_form_noof_payments_label"><p>' . $config->get('cnp.cnp_recurring_no_of_payments') . ': <b>' . t('Indefinite Recurring Only') . '</b></p>',
            '#suffix' => '</div>',
          ];
          $element['fef_cnp_noof_payments'] = [
            '#type' => 'hidden',
            '#default_value' => t('Indefinite Recurring Only'),
          ];
        }

        $element['fef_recurr_options_div_end'] = [
          '#suffix' => '</div>',
        ];

        // }
      }
      elseif ($config->get('cnp.cnp_recurr_oto')['oto'] === 0 && $config->get('cnp.cnp_recurr_recur')[1] == 1) {
        $element['fef_recurring_payment_lbl'] = [
          '#prefix' => '<div class="cnp_form_recurring_label"><p><b>' . $config->get('cnp.cnp_recurr_label') . '</b></p>',
          '#suffix' => '</div>',
        ];

        $element['fef_payment_options'] = [
          '#type' => 'hidden',
          // '#value' => $config->get('cnp.cnp_default_payment_options'),
          '#value' => 'Recurring',
        ];
        $element['fef_recurr_options_div_starts'] = [
          '#prefix' => '<div class="" id="fef_recurr_options_division">',
        ];

        // Check recurring type checkbox values. if both are checked,display
        // dropdown with both options or else display individually.
        $rto = $config->get('cnp.cnp_recurr_type_option');

        if ($rto['Installment'] === 'Installment' && $rto['Subscription'] === 'Subscription') {
          $element['cnp_form_fef_recuring_type_option_start'] = [
            '#prefix' => '<div class="cnp_form_recuring_type_option">',
          ];
          $element['fef_recuring_type_option'] = [
            '#type' => 'select',
            '#title' => $config->get('cnp.cnp_recurring_types' . '<span style="color:red"> *</span>'),
            '#options' => $rto,
            '#default_value' => $config->get('cnp.cnp_default_recurring_type'),
          ];
          $element['cnp_form_fef_recuring_type_option_end'] = [
            '#suffix' => '</div>',
          ];
        }
        else {
          $othopt = $config->get('cnp.cnp_recurr_type_option');

          if ($othopt['Installment'] === 'Installment' && $othopt['Subscription'] === 0) {
            $element['fef_recurr_options_label_display'] = [
              '#prefix' => '<div class="cnp_form_recurr_options_label"><p>' . $config->get('cnp.cnp_recurring_types') . ': ' . $othopt['Installment'] . '</p>',
              '#suffix' => '</div>',
            ];
            $element['fef_recuring_type_option'] = [
              '#type' => 'hidden',
              '#value' => $othopt['Installment'],
            ];
          }
          elseif ($othopt['Installment'] === 0 && $othopt['Subscription'] === 'Subscription') {
            $element['fef_recurr_options_label_display'] = [
              '#prefix' => '<div class="cnp_form_recurr_options_label"><p>' . $config->get('cnp.cnp_recurring_types') . ': ' . $othopt['Subscription'] . '</p>',
              '#suffix' => '</div>',
            ];
            $element['fef_recuring_type_option'] = [
              '#type' => 'hidden',
              '#value' => $othopt['Subscription'],
            ];
          }

        }
        // DISPLAY PERIODCITY: IF IT IS ONE OPTION DISPLAY DIRECTLY OR MULTIPLE
        // OPTIONS DISPLAY ALL OPTIONS AS DROPDOWN.
        $periodcityOpt = $config->get('cnp.cnp_recurring_periodicity_options');
        $periodOpt = [];
        foreach ($periodcityOpt as $popts) {
          if ($popts !== 0) {
            $periodOpt[$popts] = $popts;
          }
        }
        if (count($periodOpt) == 1) {
          $element['fef_cnp_periodcity_label_display'] = [
            '#prefix' => '<div class="cnp_form_periodcity_label"><p>' . $config->get('cnp.cnp_recurring_periodicity') . ': ' . array_values($periodOpt)[0] . '</p>',
            '#suffix' => '</div>',
          ];
          $element['fef_cnp_periodcity'] = [
            '#type' => 'hidden',
            '#value' => array_values($periodOpt)[0],
          ];

        }
        else {
          $element['cnp_form_periodcity_start'] = [
            '#prefix' => '<div class="cnp_form_periodcity">',
          ];
          $element['fef_cnp_periodcity'] = [
            '#type' => 'select',
            '#title' => $config->get('cnp.cnp_recurring_periodicity' . '<span style="color:red"> *</span>'),
            '#options' => $periodOpt,
          ];
          $element['cnp_form_periodcity_end'] = [
            '#prefix' => '</div>',
          ];
        }
        // Display default no.of payments based on no.of payments radio buttons.
        $nop = $config->get('cnp.cnp_recurring_no_of_payments_options');
        if ($nop === 'fixednumber') {
          $config->get('cnp.cnp_recurring_default_no_payment_fncc_lbl');
          $element['fef_cnp_noof_payments_label_display'] = [
            '#prefix' => '<div class="cnp_form_noof_payments_label"><p>' . $config->get('cnp.cnp_recurring_no_of_payments') . ': ' . $config->get('cnp.cnp_recurring_default_no_payments_fnnc') . '</p>',
            '#suffix' => '</div>',
          ];
          $element['fef_cnp_noof_payments'] = [
            '#type' => 'hidden',
            '#default_value' => $config->get('cnp.cnp_recurring_default_no_payments_fnnc'),
          ];
        }
        elseif ($nop === 'indefinite_openfield') {
          $element['cnp_form_noof_payments_start'] = [
            '#prefix' => '<div class="cnp_form_noof_payments">',
          ];

          $element['fef_cnp_noof_payments'] = [
            '#type' => 'textfield',
            '#title' => $config->get('cnp.cnp_recurring_no_of_payments'),
            '#default_value' => $config->get('cnp.cnp_recurring_default_no_payments_open_filed'),
          ];

          $element['cnp_form_noof_payments_end'] = [
            '#suffix' => '</div>',
          ];
        }
        elseif ($nop === 'openfield') {
          $element['cnp_form_noof_payments_start'] = [
            '#prefix' => '<div class="cnp_form_noof_payments">',
          ];
          $element['fef_cnp_noof_payments'] = [
            '#type' => 'textfield',
            '#title' => $config->get('cnp.cnp_recurring_no_of_payments'),
            '#default_value' => $config->get('cnp.cnp_recurring_default_no_payments'),
          ];
          $element['cnp_form_noof_payments_end'] = [
            '#suffix' => '</div>',
          ];
        }
        else {
          $element['fef_cnp_noof_payments_label_display'] = [
            '#prefix' => '<div class="cnp_form_noof_payments_label"><p>' . $config->get('cnp.cnp_recurring_no_of_payments') . ': <b>' . t('Indefinite Recurring Only') . '</b></p>',
            '#suffix' => '</div>',
          ];
          $element['fef_cnp_noof_payments'] = [
            '#type' => 'hidden',
            '#default_value' => t("Indefinite Recurring Only"),
          ];
        }

        $element['fef_recurr_options_div_end'] = [
          '#suffix' => '</div>',
        ];
        // }
      }
      elseif ($config->get('cnp.cnp_recurr_oto')['oto'] === "oto" && $config->get('cnp.cnp_recurr_recur')[1] == 0) {
        $element['fef_recurring_payment_lbl'] = [];
        $element['fef_payment_options'] = [
          '#type' => 'hidden',
          '#value' => t('One Time Only'),
        ];

      }
      // Wrapper to display recurring and one time form.
      $element['fef_payment_methods_lbl'] = [
        '#prefix' => '<div class=""><p></p>',
        '#suffix' => '</div>',
      ];
      // Get payment methods.
      $payOpt = [];
      $payOpt['Credit Card'] = 'Credit Card';

      $element['fef_payment_methods'] = [
        '#type' => 'hidden',
        '#value' => t('Credit Card'),
      ];

      /* ************************************************************
       ************* Credit Card Form starts Here ********************
       ***************************************************************/

      $element['fef_creditcard_div_starts'] = [
        '#prefix' => '<div class="" id="cnp-credit-card-division">',
      ];

      $cimages = [
        'Amex' => 'amex.jpg',
        'Discover' => 'Discover.jpg',
        'Jcb' => 'JCB.jpg',
        'Master' => 'mastercard.gif',
        'Visa' => 'Visa.jpg',
      ];

      $accCards = explode('#', $config->get('cnp.cnp_payment_credit_card_options_hidden'), -1);
      $ipath = '';
      foreach ($accCards as $cards) {
        if (array_key_exists($cards, $cimages)) {
          // Echo $cimages[$cards];.
          $ipath .= "<img alt='" . $cards . "' src='" . base_path() . drupal_get_path('module', 'clickandpledge_drupalcommerce') . "/images/$cimages[$cards]'>";
        }
      }

      $element['fef_creditcard_images'] = [
        '#prefix' => '<div class="cnp_accepted_payment_icons">' . $ipath . '</div>',
      ];

      $element['type'] = [
        '#type' => 'hidden',
        '#value' => '',
      ];

      $element['cnp_form_card_owner_start'] = [
        '#prefix' => '<div class="cnp_form_card_owner">',
      ];

      $element['owner'] = [
        '#type' => 'textfield',
        '#title' => t('Name on Card'),
        '#attributes' => ['autocomplete' => 'off', 'id' => 'dfs'],
        '#required' => TRUE,
        '#maxlength' => 30,
      ];
      $element['cnp_form_card_owner_end'] = [
        '#suffix' => '</div>',
      ];

      $element['cnp_form_cardnumber_start'] = [
        '#prefix' => '<div class="cnp_form_cardnumber">',
      ];
      if ($config->get('cnp.cnp_mode') == 'Yes') {
        $element['cardnumber'] = [
          '#type' => 'textfield',
          '#title' => t('Credit Card Number'),
          '#attributes' => ['autocomplete' => 'off', 'disabled' => 'disabled'],
          '#required' => TRUE,
          '#maxlength' => 16,
          '#default_value' => '4111111111111111',
        ];
      }
      else {
        $element['cardnumber'] = [
          '#type' => 'textfield',
          '#title' => t('Credit Card Number'),
          '#attributes' => ['autocomplete' => 'off'],
          '#required' => TRUE,
          '#maxlength' => 19,
        ];
      }
      $element['cnp_form_cardnumber_end'] = [
        '#suffix' => '</div>',
      ];
      $element['expiration'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['credit-card-form__expiration'],
        ],
      ];
      $element['expiration']['month'] = [
        '#type' => 'select',
        '#title' => t('Expiration Date'),
        '#options' => $months,
        '#default_value' => date('m'),
        '#required' => TRUE,
        '#prefix' => "<div class='only_expiry_date'>",
        '#suffix' => '</div>',
      ];

      $element['expiration']['year'] = [
        '#type' => 'select',
        '#title' => '',
        '#options' => $years,
        '#default_value' => $current_year_4,
        '#required' => TRUE,
        '#prefix' => "<div class='only_year'>",
        '#suffix' => '</div>',
      ];

      $element['cnp_form_security_code_start'] = [
        '#prefix' => '<div class="cnp_form_security_code">',
      ];

      $element['security_code'] = [
        '#type' => 'textfield',
        '#title' => t('Card Verification (CVV)'),
        '#attributes' => ['autocomplete' => 'off'],
        '#required' => TRUE,
        '#maxlength' => 4,
      ];
      $element['cnp_form_security_code_end'] = [
        '#suffix' => '</div>',
      ];

      $element['is_cc_enabled'] = [
        '#type' => 'hidden',
        '#value' => 'yes',
      ];
      // To display validation errors.
      $element['payment_errors'] = [
        '#type' => 'markup',
        '#markup' => '<div id="payment-errors"></div>',
        '#weight' => -200,
      ];

      $element['fef_creditcard_div_ends'] = [
        '#suffix' => '</div>',
      ];

      $element['cnp_fef_front_end_form_end'] = [
        '#suffix' => '</div>',
      ];

    }
    else {
      $element['fef_cnp_creditcard_diabled'] = [
        '#prefix' => '<div><p><b>' . t('Click&Pledge Credit Card payment gateway is Disabled') . '</b></p>',
        '#suffix' => '</div>',
      ];
      $element['is_cc_enabled'] = [
        '#type' => 'hidden',
        '#value' => 'no',
      ];
    }
    return $element;

  }

  /**
   * {@inheritdoc}
   */
  protected function validateCreditCardForm(array &$element, FormStateInterface $form_state) {
    // The JS library performs its own validation.
    $config = \Drupal::config('cnp.mainsettings');
    $values = $form_state->getValue($element['#parents']);
    $order_id = \Drupal::routeMatch()->getParameter('commerce_order')->id();
    $order = Order::load($order_id);

    if ($values['is_cc_enabled'] == 'no') {
      $form_state->setError($element['is_cc_enabled'], t('Please enable Click&Pledge payment gateway to accept payments'));
      return FALSE;
    }

    if ($values['fef_payment_methods'] == 'Credit Card') {
      if ($values['fef_payment_options'] == 'Recurring') {
        if ($values['fef_cnp_noof_payments'] != 'Indefinite Recurring Only') {
          if ($values['fef_cnp_noof_payments'] <= 0 && $values['fef_cnp_noof_payments'] != '') {
            $form_state->setError($element['fef_cnp_noof_payments'], t('No.of payments should be more than 1'));
            return FALSE;
          }
          else {
            $noofPays = $values['fef_cnp_noof_payments'];
            if ($values['fef_recuring_type_option'] == 'Installment') {
              if ($noofPays <= 1) {
                $form_state->setError($element['fef_cnp_noof_payments'], t('Please enter Number of payments value between 2 to 998 for installment'));
                return FALSE;
              }
              if ($noofPays >= 999) {
                $form_state->setError($element['fef_cnp_noof_payments'], t('Please enter Number of payments value between 2 to 998 for installment'));
                return FALSE;
              }
            }
            else {
              if ($noofPays <= 1) {
                $form_state->setError($element['fef_cnp_noof_payments'], t('Please enter Number of payments value between 2 to 999 for Subscription'));
                return FALSE;
              }
              if ($noofPays >= 1000) {
                $form_state->setError($element['fef_cnp_noof_payments'], t('Please enter Number of payments value between 2 to 999 for Subscription'));
                return FALSE;
              }
            }
            // No.of payments values should not be more than max.no.of payments
            // defiend in dashbaord.
            if ($config->get('cnp.cnp_recurring_no_of_payments_options') == "openfield") {
              $maxNo = $config->get('cnp.cnp_recurring_max_no_payment');
              if ($maxNo != "") {
                if ($maxNo != 0) {
                  if ($maxNo < $noofPays) {
                    $form_state->setError($element['fef_cnp_noof_payments'], 'Please enter Number of payments value between 2 to ' . $maxNo . ' for ' . ucfirst($values['fef_recuring_type_option']));
                    return FALSE;
                  }
                }
              }
            }
          }
        }
      }

      $accCards = explode('#', $config->get('cnp.cnp_payment_credit_card_options_hidden'), -1);
      if (in_array("Master", $accCards)) {
        $accCards[] = 'Mastercard';
      }
      // $accCards[] = 'Maestro';
      $card_type = CreditCard::detectType($values['cardnumber']);
      if (!empty($card_type)) {
        if (!in_array(ucfirst($card_type->getId()), $accCards)) {
          $form_state->setError($element['cardnumber'], $card_type->getLabel() . ' Credit Card is not supported');
          return FALSE;
        }
      }

      if (!$card_type) {
        $form_state->setError($element['cardnumber'], t('You have entered a credit card number of an unsupported card type.'));
        return;
      }
      if (!CreditCard::validateNumber($values['cardnumber'], $card_type)) {
        $form_state->setError($element['cardnumber'], t('You have entered an invalid credit card number.'));
      }
      if (!CreditCard::validateExpirationDate($values['expiration']['month'], $values['expiration']['year'])) {
        $form_state->setError($element['expiration'], t('You have entered an expired credit card.'));
      }
      if (!CreditCard::validateSecurityCode($values['security_code'], $card_type)) {
        $form_state->setError($element['security_code'], t('You have entered an invalid CVV.'));
      }
      // Persist the detected card type.
      $form_state->setValueForElement($element['type'], $card_type->getId());
    }

    // pre-authourization is unchecked.
    if ($config->get('cnp.cnp_pre_auth') == 0) {
      if (round($order->getTotalPrice()->getNumber()) == 0) {
        $form_state->setError($element['payment_errors'], t('Zero Payment not allowed.'));
        return FALSE;
      }
    }

  }

  /**
   * To get the protected property from an object.
   *
   * @param mixed $obj
   *   The object to search for a property.
   * @param string $name
   *   The property to search in object.
   *
   * @return mixed
   *   Returns the value of a property.
   */
  public function getProtectedValue($obj, $name) {
    $array = (array) $obj;
    $prefix = chr(0) . '*' . chr(0);
    return $array[$prefix . $name];
  }

  /**
   * {@inheritdoc}
   */
  public function submitCreditCardForm(array $element, FormStateInterface $form_state) {

    // The payment gateway plugin will process the submitted payment details.
    $values = $form_state->getValue($element['#parents']);
    $this->entity->card_type = $values['type'];
    $this->entity->card_number = substr($values['cardnumber'], -4);
    $this->entity->card_exp_month = $values['expiration']['month'];
    $this->entity->card_exp_year = $values['expiration']['year'];

  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    // Add the stripe attribute to the postal code field.
    $form['billing_information']['address']['widget'][0]['address_line1']['#attributes']['data-stripe'] = 'address_line1';
    $form['billing_information']['address']['widget'][0]['address_line2']['#attributes']['data-stripe'] = 'address_line2';
    $form['billing_information']['address']['widget'][0]['locality']['#attributes']['data-stripe'] = 'address_city';
    $form['billing_information']['address']['widget'][0]['postal_code']['#attributes']['data-stripe'] = 'address_zip';
    $form['billing_information']['address']['widget'][0]['country_code']['#attributes']['data-stripe'] = 'address_country';
    return $form;
  }

  /**
   * To display the payment form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns the form structure.
   */
  public function displayRecurringForm(array $form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();
    $values = $form_state->getValue($form['#parents']);
    $payOption = $values['payment_information']['add_payment_method']['payment_details']['fef_payment_options'];
    if ($payOption == 'Recurring') {
      $ajax_response->addCommand(new CssCommand('#fef_recurr_options_division', ['display' => 'block']));
    }
    else {
      $ajax_response->addCommand(new CssCommand('#fef_recurr_options_division', ['display' => 'none']));
    }
    return $ajax_response;
  }

  /**
   * To remove new line characters.
   *
   * @param mixed $str
   *   The string to be formatted.
   * @param mixed $length
   *   The length.
   * @param mixed $start
   *   The starting.
   *
   * @return mixed
   *   Returns the formatted string.
   */
  public function safeString($str, $length = 1, $start = 0) {
    // Remove new line characters.
    $str = preg_replace('/\x03/', '', $str);
    return substr(htmlspecialchars(($str)), $start, $length);
  }

}
