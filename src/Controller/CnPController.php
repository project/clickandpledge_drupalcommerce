<?php

namespace Drupal\clickandpledge_drupalcommerce\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use SimpleXMLElement;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This class will handles ajax calls.
 */
class CnPController extends ControllerBase {
  /**
   * Class Constants.
   */
  const CNPUID = '14059359-D8E8-41C3-B628-E7E030537905';
  const CNPKEY = '5DC1B75A-7EFA-4C01-BDCD-E02C536313A3';

  /**
   * The Connection.
   *
   * @var mixed
   */
  protected $connection;

  /**
   * The class Constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * AJAX call back to get the payment list.
   *
   * @param mixed $variable
   *   The account id.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns JSON data.
   */
  public function ajaxCallback($variable) {
    $data = $this->getWccnpActivePaymentList($variable);
    return new JsonResponse($data);
  }

  /**
   * The get the payment list.
   *
   * @param mixed $cnpaccid
   *   The account id.
   *
   * @return mixed
   *   Returns the payment list.
   */
  public function getWccnpActivePaymentList($cnpaccid) {
    $cnpacountid    = $cnpaccid;
    $cnpaccountGUID = $this->getwcCnpAccountGuid($cnpacountid);
    $connect1       = [
      'soap_version' => 'SOAP_1_1',
      'trace' => 1,
      'exceptions' => 0,
    ];
    $client1        = new \SoapClient('https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl', $connect1);
    if (isset($cnpacountid) && $cnpacountid != '' && isset($cnpaccountGUID) &&  $cnpaccountGUID != '') {
      $xmlr1 = new SimpleXMLElement('<GetAccountDetail></GetAccountDetail>');
      $xmlr1->addChild('accountId', $cnpacountid);
      $xmlr1->addChild('accountGUID', $cnpaccountGUID);
      $xmlr1->addChild('username', self::CNPUID);
      $xmlr1->addChild('password', self::CNPKEY);
      $response1 = $client1->GetAccountDetail($xmlr1);
      return $response1;
    }
  }

  /**
   * The accoun GUID based in account id.
   *
   * @param mixed $accid
   *   The account ID.
   *
   * @return string
   *   Returns account GUID.
   */
  public function getwcCnpAccountGuid($accid) {
    $sql = "SELECT cnpaccountsinfo_accountguid FROM {dp_cnp_dp_jbcnpaccountsinfo} where cnpaccountsinfo_orgid =:accid";
    $query = $this->connection->query($sql, [':accid' => $accid]);
    $result = $query->fetchAssoc();
    return $result['cnpaccountsinfo_accountguid'];

  }

  /**
   * To get Account List based on email.
   *
   * @param mixed $cnpacid
   *   The account ID.
   *
   * @return array
   *   Returns accouont list array.
   */
  public function getcnpUserEmailAccountList($cnpacid) {
    $cnpwcaccountid = $cnpacid;
    $totRes = [];

    $cnprtrntxt = $this->getwccnpConnectCampaigns($cnpwcaccountid);
    $totRes[] = $cnprtrntxt;
    $clist = [];
    $cnprtrnpaymentstxt = $this->getWCCnPactivePaymentList($cnpwcaccountid);
    foreach ($cnprtrnpaymentstxt as $obj => $cli) {
      foreach ($cli as $key => $value) {
        if ($value == 1) {
          $clist[$key] = $key;
        }
      }
    }
    $totRes[] = $clist;
    return $totRes;
  }

  /**
   * To get the campaigns list.
   *
   * @param mixed $cnpaccid
   *   The account id.
   *
   * @return array
   *   Returns All Campaigns.
   */
  public function getwccnpConnectCampaigns($cnpaccid) {

    $cnpacountid = $cnpaccid;
    $cnpaccountGUID = $this->getwcCnpAccountGuid($cnpacountid);
    $connect = ['soap_version' => 'SOAP_1_1', 'trace' => 1, 'exceptions' => 0];
    $client = new \SoapClient('https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl', $connect);
    if (isset($cnpacountid) && $cnpacountid != '' && isset($cnpaccountGUID) &&  $cnpaccountGUID != '') {
      $xmlr = new SimpleXMLElement('<GetActiveCampaignList2></GetActiveCampaignList2>');
      $xmlr->addChild('accountId', $cnpacountid);
      $xmlr->addChild('AccountGUID', $cnpaccountGUID);
      $xmlr->addChild('username', self::CNPUID);
      $xmlr->addChild('password', self::CNPKEY);

      $response = $client->GetActiveCampaignList2($xmlr);
      $responsearr = $response->GetActiveCampaignList2Result->connectCampaign;

    }
    else {
      $responsearr = (object) ['' => 'No Records Found'];
    }
    return $responsearr;

  }

  /**
   * To get the updated data of the accounts.
   *
   * @param mixed $variable
   *   The account ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns JSON data with updated data.
   */
  public function getRefreshAccounts($variable) {
    $rtnrefreshtokencnpdata = $this->getRefreshToken();
    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_URL => 'https://aaas.cloud.clickandpledge.com/IdServer/connect/token',
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => $rtnrefreshtokencnpdata,
      CURLOPT_HTTPHEADER => [
        'cache-control: no-cache',
        'content-type: application/x-www-form-urlencoded',
      ],
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);
    if ($err) {
      echo "cURL Error #:" . $err;
    }
    else {
      $cnptokendata = json_decode($response);
      // var_dump($cnptokendata);
      // exit;
      $cnptoken = $cnptokendata->access_token;
      $cnprtokentyp = $cnptokendata->token_type;
      if ($cnptoken != '') {
        $curl = curl_init();

        curl_setopt_array($curl, [
          CURLOPT_URL => 'https://api.cloud.clickandpledge.com/users/accountlist',
          CURLOPT_RETURNTRANSFER => TRUE,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => [
            'accept: application/json',
            "authorization: " . $cnprtokentyp . " " . $cnptoken,
            'content-type: application/json',
          ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        }
        else {

          $cnpAccountsdata = json_decode($response);
          $rtncnpdata = $this->deleteCnpAccountsList();

          $totalAccounts = [];
          foreach ($cnpAccountsdata as $cnpkey => $cnpvalue) {
            $acc = [];
            // Data come form service-insert data into accounts list table.
            $cnporgid = $cnpvalue->OrganizationId;
            $cnporgname = addslashes($cnpvalue->OrganizationName);
            $cnpaccountid = $cnpvalue->AccountGUID;
            $cnpufname = addslashes($cnpvalue->UserFirstName);
            $cnplname = addslashes($cnpvalue->UserLastName);
            $cnpuid = $cnpvalue->UserId;
            $rtncnpdata = $this->insertCnpwcAccountsList($cnporgid, $cnporgname, $cnpaccountid, $cnpufname, $cnplname, $cnpuid);
            $acc['orgid'] = $cnporgid;
            $acc['orgname'] = $cnporgname;
            $totalAccounts[] = $acc;
          }
        }

      }
    }

    return new JsonResponse($totalAccounts);
  }

  /**
   * To get the refresh token from db table.
   *
   * @return string
   *   Returns refresh token.
   */
  public function getRefreshToken() {
    $sql = 'SELECT cnptokeninfo_refreshtoken FROM {dp_cnp_dp_jbcnptokeninfo}';
    $query = $this->connection->query($sql);
    $result = $query->fetchAssoc();
    $refreshtoken = $result['cnptokeninfo_refreshtoken'];
    $sql1 = 'SELECT cnpsettingsinfo_clentsecret, cnpsettingsinfo_clientid, cnpsettingsinfo_scope  FROM {dp_cnp_dp_jbcnpsettingsinfo}';
    $query1 = $this->connection->query($sql1);
    $result1 = $query1->fetchAssoc();
    $password = 'password';
    $cnpsecret = openssl_decrypt($result1['cnpsettingsinfo_clentsecret'], "AES-128-ECB", $password);
    $rtncnpdata = "client_id=" . $result1['cnpsettingsinfo_clientid'] . "&client_secret=" . $cnpsecret . "&grant_type=refresh_token&scope=" . $result1['cnpsettingsinfo_scope'] . "&refresh_token=" . $refreshtoken;
    return $rtncnpdata;
  }

  /**
   * TO delete account list.
   */
  public function deleteCnpAccountsList() {
    $this->connection->delete('dp_cnp_dp_jbcnpaccountsinfo')->execute();
  }

  /**
   * To get the account lists.
   *
   * @param mixed $cnporgid
   *   The organization id.
   * @param mixed $cnporgname
   *   Organization name.
   * @param mixed $cnpaccountid
   *   Account id.
   * @param mixed $cnpufname
   *   User first name.
   * @param mixed $cnplname
   *   User last name.
   * @param mixed $cnpuid
   *   User id.
   *
   * @return mixed
   *   Returns Inserted id.
   */
  public function insertCnpwcAccountsList($cnporgid, $cnporgname, $cnpaccountid, $cnpufname, $cnplname, $cnpuid) {
    $this->connection->insert('dp_cnp_dp_jbcnpaccountsinfo')
      ->fields([
        'cnpaccountsinfo_orgid' => $cnporgid,
        'cnpaccountsinfo_orgname' => $cnporgname,
        'cnpaccountsinfo_accountguid' => $cnpaccountid,
        'cnpaccountsinfo_userfirstname' => $cnpufname,
        'cnpaccountsinfo_userlastname' => $cnplname,
        'cnpaccountsinfo_userid' => $cnpuid,
      ])
      ->execute();
    $res = $this->connection->query("select max(cnpaccountsinfo_id) from {dp_cnp_dp_jbcnpaccountsinfo}");
    $id = $res->fetchCol();
    return $id[0];
  }

  /**
   * Sign in with different account.
   */
  public function signinDifferentUser() {
    \Drupal::configFactory()->getEditable('cnp.settings')->delete();
    $this->cnpPageRedirect('cnpauth');
  }

  /**
   * Page redirection.
   *
   * @param mixed $path
   *   The URL to redirect.
   */
  public function cnpPageRedirect($path) {
    $response = new RedirectResponse($path, 302);
    $response->send();
  }

}
