<?php

namespace Drupal\clickandpledge_drupalcommerce\Plugin\Commerce\PaymentGateway;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Serialization\Yaml;
use DOMDocument;
use Exception;

/**
 * Provides the Test payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "cnpmoney_redirect",
 *   label = "Click&Pledge CreditCard",
 *   display_label = "Click&Pledge CreditCard",
 *   forms = {
 *     "add-payment-method" = "Drupal\clickandpledge_drupalcommerce\PluginForm\CnPCommerceRedirect\PaymentCnPMoneyForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   },
 *   js_library = "clickandpledge_drupalcommerce/js",
 * )
 */
class CnPCommerceRedirect extends OnsitePaymentGatewayBase {

  /**
   * The Order.
   *
   * @var mixed
   */
  protected $order;

  /**
   * The Order transaction.
   *
   * @var mixed
   */
  protected $orderTransation;

  /**
   * The amount.
   *
   * @var mixed
   */
  protected $amount;

  /**
   * The Payment.
   *
   * @var mixed
   */
  protected $payment;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $current_path = \Drupal::service('path.current')->getPath();
    $args = explode("/", $current_path);
    foreach ($args as $key => $value) {
      if (is_numeric($value)) {
        $orderId = $value;
        $this->order = Order::load($orderId);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    return $this->crateCnpConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Validations are doing by using script.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Form submission done by using ajax calls.
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $additionalCharges = $request->get('additionalCharges');
    $status = $request->get('status');
    $firstname = $request->get('firstname');
    $txnid = $request->get('txnid');
    $amount = $request->get('amount');
    $posted_hash = $request->get('hash');
    $key = $request->get('key');
    $productinfo = $request->get('productinfo');
    $email = $request->get('email');
    $salt = $this->configuration['cnpsalt'];
    if (isset($additionalCharges)) {
      $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
    }
    else {
      $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
    }
    $hash = hash("sha512", $retHashSeq);
    if ($hash != $posted_hash) {
      $this->messenger()->addError($this->t('Invalid Transaction. Please try again'));
    }
    else {
      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      $payment = $payment_storage->create([
        'state' => $status,
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => $this->entityId,
        'order_id' => $order->id(),
        'test' => $this->getMode() == 'test',
        'remote_id' => $txnid,
        'remote_state' => $request->get('payment_status'),
        'authorized' => REQUEST_TIME,
      ]);
      $payment->save();
      $this->messenger()->addMessage($this->t('Your payment was successful with Order id : @orderid and Transaction id : @transaction_id', ['@orderid' => $order->id(), '@transaction_id' => $txnid]));
    }
  }

  /**
   * To get the shipment information.
   *
   * @return object
   *   Return shipment information
   */
  protected function getShipment() {
    if ($this->order->hasField('shipments')) {
      $shipments = $this->order->get('shipments');
      if ($shipments->isEmpty()) {
        // Temporary code to create a new shipment.
        $shipment = $this->createShipment();
        $shipments->entity = $shipment;
        $this->order->save();
      }
      else {
        $shipment = $shipments->entity;
      }
      return $shipment;
    }
    else {
      return '';
    }
  }
  
  /**
   * To get the protected value of an object.
   *
   * @param mixed $obj
   *   The object to be checked.
   * @param mixed $name
   *   The name of property in the object.
   *
   * @return mixed
   *   Returns the value of property.
   */
  public function getProtectedValue($obj, $name) {
    $array = (array) $obj;
    $prefix = chr(0) . '*' . chr(0);
    return $array[$prefix . $name];
  }

  /**
   * To get the shipping plug in name.
   *
   * @return string
   *   Returns the name of the plug in
   */
  public function getShippingPluginType() {
    if ($this->order->hasField('shipments')) {
      $ship_entity = $this->order->shipments->entity->shipping_method->entity;
      $data = $this->getProtectedValue($ship_entity, 'values');
      if (count($data)) {
        return $data['plugin']['x-default']['target_plugin_id'];
      }
      else {
        return '';
      }
    }
    else {
      return '';
    }
  }

  /**
   * To get the shipping plug in name.
   *
   * @return string
   *   Returns the name of the plug in
   */
  public function getShippingPluginName() {
    if ($this->order->hasField('shipments')) {
      $ship_entity = $this->order->shipments->entity->shipping_method->entity;
      $data = $this->getProtectedValue($ship_entity, 'values');
      if (count($data)) {
        return $data['name']['x-default'];
      }
      else {
        return '';
      }
    }
    else {
      return '';
    }
  }

  /**
   * To check whether the shipping cost is zero or not.
   *
   * @return boolen
   *   Returns true or false
   */
  public function isShippingZero() {
    if ($this->order->hasField('shipments')) {
      $ship_entity = $this->order->shipments->entity->shipping_method->entity;
      $data = $this->getProtectedValue($ship_entity, 'values');
      $sprice = $data['plugin']['x-default']['target_plugin_configuration']['rate_amount']['number'];
      if ($sprice <= 0 || $sprice == 0) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * Create payment Data as an XML and submits the same to API.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The Payment object.
   * @param bool $capture
   *   The payment capture.
   *
   * @throws \Exception
   *   Throws an exception if anything wrong.
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    if ($this->order->getTotalPrice()->isZero()) {
      $pM = '';
    }
    else {
      $pM = '';
    }
    $amount = $payment->getAmount();
    $transaction_data = [
      'currency' => $amount->getCurrencyCode(),
      'amount' => $this->toMinorUnits($amount),
      'source' => $pM,
      'capture' => $capture,
      'customer' => '',
    ];
    /*if (!$this->order->getTotalPrice()->isZero()) {
    $owner = $payment_method->getOwner();
    if ($owner && $owner->isAuthenticated()) {
    $transaction_data['customer'] = $this->getRemoteCustomerId($owner);
    }
    }*/
    $payment_details = $_SESSION['payment_details'];
    $amount = $payment->getAmount()->getNumber();
    $month = (string) $payment_details['expiration']['month'];
    $year = (string) $payment_details['expiration']['year'];
    $year = substr($year, -2);
    $exp_date = $month . $year;
    $connection = \Drupal::database();
    $config = \Drupal::config('cnp.mainsettings');
    if ($config->get('cnp.cnp_pre_auth') == 1 && $payment_details['fef_payment_options'] == 'Recurring') {

      if ($this->order->getTotalPrice()->getNumber() == 0) {
        try {
          if ($this->order->getTotalPrice()->getNumber() == 0 && $this->isShippingZero()) {
            throw new Exception('Zero Payment not allowed with Recurring.Please select One Time Only');
          }
        }
        catch (Exception $e) {
          $this->messenger()->addError($e->getMessage());
          $this->redirectToPreviousStep();
        }
      }
    }
    if ($config->get('cnp.cnp_pre_auth') == 0) {
      if ($this->order->getTotalPrice()->getNumber() == 0) {
        try {
          if ($this->order->getTotalPrice()->getNumber() == 0) {
            throw new Exception('Zero Payment not allowed');
          }
        }
        catch (Exception $e) {
          // drupal_set_message($e->getMessage(), 'error');.
          $this->messenger()->addError($e->getMessage());
          $this->redirectToPreviousStep();
        }
      }
    }

    $shipment = $this->getShipment();
    if (!empty($shipment)) {
      $shipping_profile = $shipment->getShippingProfile();
    }
    $address = $this->order->getBillingProfile()->address->first();
    $dom = new DOMDocument('1.0', 'UTF-8');
    $root = $dom->createElement('CnPAPI', '');
    $root->setAttribute('xmlns', "urn:APISchema.xsd");
    $root = $dom->appendChild($root);
    $version = $dom->createElement('Version', '1.0');
    $version = $root->appendChild($version);
    $engine = $dom->createElement('Engine', '');
    $engine = $root->appendChild($engine);
    $application = $dom->createElement('Application', '');
    $application = $engine->appendChild($application);
    $applicationid = $dom->createElement('ID', 'CnP_Drupal_Commerce');
    $applicationid = $application->appendChild($applicationid);
    $applicationname = $dom->createElement('Name', 'CnP_Drupal_Commerce');
    $applicationid = $application->appendChild($applicationname);
    // Getting drupal Version.
    $drupalVersion = \Drupal::VERSION;
    // Getting Commerce Version.
    $path = drupal_get_path('module', 'commerce') . '/commerce.info.yml';
    if (file_exists($path)) {
      $file_path = DRUPAL_ROOT . '/';
      $fullpath = $file_path . $path;
      $file_contents = file_get_contents($fullpath);
      $ymldata = Yaml::decode($file_contents);
      $commerceVersion = $ymldata['version'];
    }
    else {
      $commerceVersion = '';
    }
    $versionString = "03.2008100000/Drupal:v" . $drupalVersion . "/DrupalCommerce:v" . $commerceVersion;
    $applicationversion = $dom->createElement('Version', $versionString);
    $applicationversion = $application->appendChild($applicationversion);
    $request = $dom->createElement('Request', '');
    $request = $engine->appendChild($request);
    $operation = $dom->createElement('Operation', '');
    $operation = $request->appendChild($operation);
    $operationtype = $dom->createElement('OperationType', 'Transaction');
    $operationtype = $operation->appendChild($operationtype);
    $ipaddress = $dom->createElement('IPAddress', $this->order->getIpAddress());
    $ipaddress = $operation->appendChild($ipaddress);
    $httpreferrer = $dom->createElement('UrlReferrer', $_SERVER['HTTP_REFERER']);
    $httpreferrer = $operation->appendChild($httpreferrer);
    $authentication = $dom->createElement('Authentication', '');
    $authentication = $request->appendChild($authentication);
    // Getting account guid form database table.
    $accid = $config->get('cnp.cnp_accid');
    $sql = "SELECT cnpaccountsinfo_accountguid FROM {dp_cnp_dp_jbcnpaccountsinfo} where cnpaccountsinfo_orgid=:accid";
    $query = $connection->query($sql, [':accid' => $accid]);
    $row = $query->fetchAll();
    $accounttype = $dom->createElement('AccountGuid', $row[0]->cnpaccountsinfo_accountguid);
    $accounttype = $authentication->appendChild($accounttype);
    $accountid = $dom->createElement('AccountID', $config->get('cnp.cnp_accid'));
    $accountid = $authentication->appendChild($accountid);
    $cporder = $dom->createElement('Order', '');
    $cporder = $request->appendChild($cporder);
    if ($config->get('cnp.cnp_mode') == 'No') {
      $orderMode = 'Production';
    }
    else {
      $orderMode = 'Test';
    }
    $ordermode = $dom->createElement('OrderMode', $orderMode);
    $ordermode = $cporder->appendChild($ordermode);
    if ($config->get('cnp.cnp_camp_urls')) {
      $camp_url = $config->get('cnp.cnp_camp_urls');
    }
    else {
      $camp_url = "";
    }
    $connectcampalias = $dom->createElement('ConnectCampaignAlias', $this->safeString($camp_url, 50));
    $connectcampalias = $cporder->appendChild($connectcampalias);

    $cardholder = $dom->createElement('CardHolder', '');
    $cardholder = $cporder->appendChild($cardholder);
    $billinginfo = $dom->createElement('BillingInformation', '');
    $billinginfo = $cardholder->appendChild($billinginfo);

    $billfirst_name = $dom->createElement('BillingFirstName', '');
    $billfirst_name = $billinginfo->appendChild($billfirst_name);
    $billfirst_name->appendChild($dom->createCDATASection($this->safeString($address->getGivenName(), 50)));

    $billlast_name = $dom->createElement('BillingLastName', '');
    $billlast_name = $billinginfo->appendChild($billlast_name);
    $billlast_name->appendChild($dom->createCDATASection($this->safeString($address->getFamilyName(), 50)));

    $bill_email = $dom->createElement('BillingEmail', $this->order->getEmail());
    $bill_email = $billinginfo->appendChild($bill_email);

    $billingaddress = $dom->createElement('BillingAddress', '');
    $billingaddress = $cardholder->appendChild($billingaddress);

    $billingaddress1 = $dom->createElement('BillingAddress1', '');
    $billingaddress1 = $billingaddress->appendChild($billingaddress1);
    $billingaddress1->appendChild($dom->createCDATASection($this->safeString($address->getAddressLine1(), 100)));

    if (!empty($address->getAddressLine2())) {
      $billingaddress2 = $dom->createElement('BillingAddress2', '');
      $billingaddress2 = $billingaddress->appendChild($billingaddress2);
      $billingaddress2->appendChild($dom->createCDATASection($this->safeString($address->getAddressLine2(), 100)));
    }
    if (!empty($address->getLocality())) {
      $billing_city = $dom->createElement('BillingCity', '');
      $billing_city = $billingaddress->appendChild($billing_city);
      $billing_city->appendChild($dom->createCDATASection($this->safeString($address->getLocality(), 50)));
    }
    if (!empty($address->getAdministrativeArea())) {
      $billing_state = $dom->createElement('BillingStateProvince', '');
      $billing_state = $billingaddress->appendChild($billing_state);
      $billing_state->appendChild($dom->createCDATASection($this->safeString($address->getAdministrativeArea(), 50)));
    }
    if (!empty($address->getPostalCode())) {
      $billing_zip = $dom->createElement('BillingPostalCode', '');
      $billing_zip = $billingaddress->appendChild($billing_zip);
      $billing_zip->appendChild($dom->createCDATASection($this->safeString($address->getPostalCode(), 20)));
    }
    $modulePath = \Drupal::moduleHandler()->getModule('clickandpledge_drupalcommerce')->getPath();
    $host = \Drupal::request()->getHost();
    $countries = simplexml_load_file("http://" . $host . "/" . base_path() . $modulePath . "/countries/Countries.xml");
    $billing_country_id = '';
    foreach ($countries as $country) {
      if ($country->attributes()->Abbrev == $address->getCountryCode()) {
        $billing_country_id = $country->attributes()->Code;
      }
    }
    if (!empty($address->getCountryCode())) {
      $billing_country = $dom->createElement('BillingCountryCode', str_pad($billing_country_id, 3, "0", STR_PAD_LEFT));
      $billing_country = $billingaddress->appendChild($billing_country);
    }
    // If shipping address found add to XML.
    if (!empty($shipment)) {
      if ($shipment->getAmount()->getNumber() != '') {
        $givenName = $shipping_profile->get('address')->first()->getGivenName();
        $getFamilyName = $shipping_profile->get('address')->first()->getFamilyName();
        $getAddressLine1 = $shipping_profile->get('address')->first()->getAddressLine1();
        $getAddressLine2 = $shipping_profile->get('address')->first()->getAddressLine2();
        $getCity = $shipping_profile->get('address')->first()->getLocality();

        $shippinginfo = $dom->createElement('ShippingInformation', '');
        $shippinginfo = $cardholder->appendChild($shippinginfo);
        // Newly Added.
        $shippingContactInformation = $dom->createElement('ShippingContactInformation', '');
        $shippingContactInformation = $shippinginfo->appendChild($shippingContactInformation);
        if ($givenName != '') {
          $shipping_first_name = $dom->createElement('ShippingFirstName', '');
          $shipping_first_name = $shippingContactInformation->appendChild($shipping_first_name);
          $shipping_first_name->appendChild($dom->createCDATASection($this->safeString($givenName, 50)));
        }
        if ($getFamilyName != '') {
          $shipping_last_name = $dom->createElement('ShippingLastName', '');
          $shipping_last_name = $shippingContactInformation->appendChild($shipping_last_name);
          $shipping_last_name->appendChild($dom->createCDATASection($this->safeString($getFamilyName, 50)));
        }
        $shippingaddress = $dom->createElement('ShippingAddress', '');
        $shippingaddress = $shippinginfo->appendChild($shippingaddress);
        if ($getAddressLine1 != '') {
          $ship_address1 = $dom->createElement('ShippingAddress1', '');
          $ship_address1 = $shippingaddress->appendChild($ship_address1);
          $ship_address1->appendChild($dom->createCDATASection($this->safeString($getAddressLine1, 100)));
        }
        if ($getAddressLine2 != '') {
          $ship_address2 = $dom->createElement('ShippingAddress2', '');
          $ship_address2 = $shippingaddress->appendChild($ship_address2);
          $ship_address2->appendChild($dom->createCDATASection($this->safeString($getAddressLine2, 100)));
        }
        if ($getCity != '') {
          $ship_city = $dom->createElement('ShippingCity', '');
          $ship_city = $shippingaddress->appendChild($ship_city);
          $ship_city->appendChild($dom->createCDATASection($this->safeString($getCity, 50)));
        }
        $getAdministrativeArea = $shipping_profile->get('address')->first()->getAdministrativeArea();
        if ($getAdministrativeArea != '') {
          $ship_state = $dom->createElement('ShippingStateProvince', '');
          $ship_state = $shippingaddress->appendChild($ship_state);
          $ship_state->appendChild($dom->createCDATASection($this->safeString($getAdministrativeArea, 50)));
        }
        $getPostalCode = $shipping_profile->get('address')->first()->getPostalCode();
        if ($getPostalCode != '') {
          $ship_zip = $dom->createElement('ShippingPostalCode', '');
          $ship_zip = $shippingaddress->appendChild($ship_zip);
          $ship_zip->appendChild($dom->createCDATASection($this->safeString($getPostalCode, 20)));
        }
        $getCountryCode = $shipping_profile->get('address')->first()->getCountryCode();
        $shipping_country_id = '';
        foreach ($countries as $country) {
          if ($country->attributes()->Abbrev == $getCountryCode) {
            $shipping_country_id = $country->attributes()->Code;
          }
        }
        if ($getCountryCode != '') {
          $ship_country = $dom->createElement('ShippingCountryCode', str_pad($shipping_country_id, 3, '0', STR_PAD_LEFT));
          $ship_country = $shippingaddress->appendChild($ship_country);
        }
      }
    }
    // Billing company name.
    if ($address->getOrganization() != '') {
      $custom_fields['Billing Company Name'] = $address->getOrganization();
    }
    // Shipping company name.
    if (!empty($shipment)) {
      if ($shipping_profile->get('address')->first()->getOrganization() != '') {
        $custom_fields['Shipping Company Name'] = $shipping_profile->get('address')->first()->getOrganization();
      }
    }
    if (count($custom_fields) > 0) {
      $customfieldlist = $dom->createElement('CustomFieldList', '');
      $customfieldlist = $cardholder->appendChild($customfieldlist);
      foreach ($custom_fields as $cfk => $cfv) {
        $customfield = $dom->createElement('CustomField', '');
        $customfield = $customfieldlist->appendChild($customfield);
        $fieldname = $dom->createElement('FieldName', '');
        $fieldname = $customfield->appendChild($fieldname);
        $fieldname->appendChild($dom->createCDATASection($this->safeString($cfk, 200)));
        $fieldvalue = $dom->createElement('FieldValue', '');
        $fieldvalue = $customfield->appendChild($fieldvalue);
        $fieldvalue->appendChild($dom->createCDATASection($this->safeString($cfv, 500)));
      }
    }
    $paymentmethod = $dom->createElement('PaymentMethod', '');
    $paymentmethod = $cardholder->appendChild($paymentmethod);
    $paymentType = $payment_details['fef_payment_methods'];
    if ($paymentType == 'Credit Card') {
      $payment_type = $dom->createElement('PaymentType', 'CreditCard');
      $payment_type = $paymentmethod->appendChild($payment_type);
      $creditcard = $dom->createElement('CreditCard', '');
      $creditcard = $paymentmethod->appendChild($creditcard);

      if ($payment_details['owner'] != '') {
        $credit_name = $dom->createElement('NameOnCard', '');
        $credit_name = $creditcard->appendChild($credit_name);
        $credit_name->appendChild($dom->createCDATASection($this->safeString($payment_details['owner'], 50)));
      }
      else {
        $fname = $address->getGivenName();
        $lname = $address->getFamilyName();
        $owner = $fname . " " . $lname;
        $credit_name = $dom->createElement('NameOnCard', '');
        $credit_name = $creditcard->appendChild($credit_name);
        $credit_name->appendChild($dom->createCDATASection($this->safeString($owner, 50)));
      }
      $month = (string) $payment_details['expiration']['month'];
      $year = (string) $payment_details['expiration']['year'];
      $year = substr($year, -2);
      $exp_date = $month . "/" . $year;
      $credit_number = $dom->createElement('CardNumber', $this->safeString(str_replace(' ', '', $payment_details['cardnumber']), 19));
      $credit_number = $creditcard->appendChild($credit_number);
      $credit_cvv = $dom->createElement('Cvv2', $payment_details['security_code']);
      $credit_cvv = $creditcard->appendChild($credit_cvv);
      $credit_expdate = $dom->createElement('ExpirationDate', $exp_date);
      $credit_expdate = $creditcard->appendChild($credit_expdate);
    }
    elseif ($paymentType == 'eCheck') {
      $payment_type = $dom->createElement('PaymentType', 'Check');
      $payment_type = $paymentmethod->appendChild($payment_type);
      $echeck = $dom->createElement('Check', '');
      $echeck = $paymentmethod->appendChild($echeck);
      $ecAccount = $dom->createElement('AccountNumber', $this->safeString($payment_details['cnp_fef_account_number'], 17));
      $ecAccount = $echeck->appendChild($ecAccount);
      $ecAccount_type = $dom->createElement('AccountType', $values['cnp_fef_account_type']);
      $ecAccount_type = $echeck->appendChild($ecAccount_type);
      $ecRouting = $dom->createElement('RoutingNumber', $this->safeString($payment_details['cnp_fef_routing_number'], 9));
      $ecRouting = $echeck->appendChild($ecRouting);
      $ecCheck = $dom->createElement('CheckNumber', $this->safeString($payment_details['cnp_fef_check_number'], 10));
      $ecCheck = $echeck->appendChild($ecCheck);
      $ecChecktype = $dom->createElement('CheckType', $payment_details['cnp_fef_check_type']);
      $ecChecktype = $echeck->appendChild($ecChecktype);
      $ecName = $dom->createElement('NameOnAccount', $this->safeString($payment_details['cnp_fef_name_on_account'], 100));
      $ecName = $echeck->appendChild($ecName);
      $ecIdtype = $dom->createElement('IdType', $values['cnp_fef_type_of_id']);
      $ecIdtype = $echeck->appendChild($ecIdtype);
    }
    else {
      $payment_type = $dom->createElement('PaymentType', 'CustomPaymentType');
      $payment_type = $paymentmethod->appendChild($payment_type);
      $customPayment = $dom->createElement('CustomPaymentType', '');
      $customPayment = $paymentmethod->appendChild($customPayment);
      $customPaymentNum = $dom->createElement('CustomPaymentNumber', $this->safeString($payment_details['cnp_fef_reference_code'], 50));
      $customPaymentNum = $customPayment->appendChild($customPaymentNum);
    }
    $orderitemlist = $dom->createElement('OrderItemList', '');
    $orderitemlist = $cporder->appendChild($orderitemlist);
    // Iterate Order Items.
    $unitPriceCalculate = $unitTaxCalculate = $shippingValueCalculate = $shippingTaxCalculate = $totalDiscountCalculate = 0;
    $pi = 103000;
    $grandTotalPrice = '';
    foreach ($this->order->getItems() as $it) {
      ++$pi;
      $pv = $it->getPurchasedEntity();
      $adjustments = $it->getAdjustments();
      $negative_tax_adjustments = array_filter($adjustments, function ($adjustment) {
        return $adjustment->getType() == 'tax' && $adjustment->isNegative();
      });
      $adjustments = array_diff_key($adjustments, $negative_tax_adjustments);
      $orderitem = $dom->createElement('OrderItem', '');
      $orderitem = $orderitemlist->appendChild($orderitem);
      $itemid = $dom->createElement('ItemID', $pi);
      $itemid = $orderitem->appendChild($itemid);

      $itemname = $dom->createElement('ItemName', '');
      $itemname = $orderitem->appendChild($itemname);
      $itemname->appendChild($dom->createCDATASection($this->safeString($it->get('title')->value, 100)));

      $quntity = $dom->createElement('Quantity', abs($it->get('quantity')->value));
      $quntity = $orderitem->appendChild($quntity);
      // Recurring calculation for unit price.
      if ($payment_details['fef_payment_options'] == 'Recurring') {
        if ($payment_details['fef_recuring_type_option'] == 'Installment') {
          // Installment.
          if ($payment_details['fef_cnp_noof_payments'] == 'Indefinite Recurring Only') {
            $no_of_payments = $payment_details['fef_cnp_noof_payments'];
            $unit_Price = ($this->numberFormatprc(($pv->getPrice()->getNumber() / 999), 2, '.', '') * 100);
            $unitPriceCalculate += ($this->numberFormatprc(($pv->getPrice()->getNumber() / 999), 2, '.', '') * $it->get('quantity')->value);
          }
          else {
            // For selected no.of payments.
            $no_of_payments = $payment_details['fef_cnp_noof_payments'];
            $unit_Price = ($this->numberFormatprc(($pv->getPrice()->getNumber() / $no_of_payments), 2, '.', '') * 100);
            $unitPriceCalculate += ($this->numberFormatprc(($pv->getPrice()->getNumber() / $no_of_payments), 2, '.', '') * $it->get('quantity')->value);
          }
          $unitprice = $dom->createElement('UnitPrice', $unit_Price);
          $unitprice = $orderitem->appendChild($unitprice);
        }
        else {
          // Subscription.
          $unitprice = $dom->createElement('UnitPrice', $this->numberFormatprc($pv->getPrice()->getNumber(), 2, '.', '') * 100);
          $unitprice = $orderitem->appendChild($unitprice);
          $unitPriceCalculate += ($pv->getPrice()->getNumber() * $it->get('quantity')->value);
        }
      }
      else {
        // One Time Only.
        $unitprice = $dom->createElement('UnitPrice', $this->numberFormatprc($pv->getPrice()->getNumber(), 2, '.', '') * 100);
        $unitprice = $orderitem->appendChild($unitprice);
        $unitPriceCalculate += ($pv->getPrice()->getNumber() * $it->get('quantity')->value);
      }
      // Recurring calculation for Tax and Discounts.
      if (count($adjustments) > 0) {
        $totalTax = 0;
        $totalDiscount = 0;
        $unitD = 0;
        foreach ($adjustments as $adjustment) {
          if ($adjustment->getType() == 'tax') {
            // Echo $adjustment->getType();
            $indTax = $adjustment->getAmount()->getNumber();
            $percentage = $adjustment->getPercentage() * 100;
            if ($payment_details['fef_payment_options'] == 'Recurring') {
              if ($payment_details['fef_recuring_type_option'] == 'Installment') {
                // Installment.
                if ($payment_details['fef_cnp_noof_payments'] == 'Indefinite Recurring Only') {
                  $no_of_payments = $payment_details['fef_cnp_noof_payments'];
                  $unit_Tax = $this->numberFormatprc((($indTax / $it->get('quantity')->value) / 999), 2, '.', '') * 100;
                  $unitTaxCalculate += ($this->numberFormatprc((($indTax / $it->get('quantity')->value) / 999), 2, '.', '') * $it->get('quantity')->value);
                }
                else {
                  // For selected no.of payments.
                  $no_of_payments = $payment_details['fef_cnp_noof_payments'];
                  $unit_Tax = $this->numberFormatprc((($indTax / $it->get('quantity')->value) / $no_of_payments), 2, '.', '') * 100;
                  $unitTaxCalculate += ($this->numberFormatprc((($indTax / $it->get('quantity')->value) / $no_of_payments), 2, '.', '') * $it->get('quantity')->value);
                }
                $itemtax = $dom->createElement('UnitTax', $unit_Tax);
                $itemtax = $orderitem->appendChild($itemtax);
              }
              else {
                // If subscription.
                $no_of_payments = $payment_details['fef_cnp_noof_payments'];
                $unit_Tax = $this->numberFormatprc(($indTax / $it->get('quantity')->value), 2, '.', '') * 100;
                $itemtax = $dom->createElement('UnitTax', $unit_Tax);
                $itemtax = $orderitem->appendChild($itemtax);
                $unitTaxCalculate += ($this->numberFormatprc($indTax / $it->get('quantity')->value, 2, '.', '') * $it->get('quantity')->value);
              }
            }
            else {
              $itemtax = $dom->createElement('UnitTax', $this->numberFormatprc($indTax / $it->get('quantity')->value, 2, '.', '') * 100);
              $itemtax = $orderitem->appendChild($itemtax);
              $unitTaxCalculate += ($this->numberFormatprc($indTax / $it->get('quantity')->value, 2, '.', '') * $it->get('quantity')->value);
            }
          }
        }
        foreach ($adjustments as $adjustment) {
          // Discount calculation $totalDiscountCalculate=0.
          if ($adjustment->getType() == 'promotion') {
            // Echo $adjustment->getType();
            $indItemDiscount = abs($adjustment->getAmount()->getNumber());
            // Echo $indItemDiscount;
            // exit;.
            if ($payment_details['fef_payment_options'] == 'Recurring') {
              if ($payment_details['fef_recuring_type_option'] == 'Installment') {
                // Installment.
                if ($payment_details['fef_cnp_noof_payments'] == 'Indefinite Recurring Only') {
                  $no_of_payments = $payment_details['fef_cnp_noof_payments'];
                  $unit_Discount = $this->numberFormatprc(($indItemDiscount / 999), 2, '.', '') * 100;
                  $totalDiscountCalculate += ($this->numberFormatprc((($indItemDiscount / $it->get('quantity')->value) / 999), 2, '.', '') * $it->get('quantity')->value);
                }
                else {
                  // For selected no.of payments.
                  $no_of_payments = $payment_details['fef_cnp_noof_payments'];
                  $unit_Discount = $this->numberFormatprc((($indItemDiscount / $it->get('quantity')->value) / $no_of_payments), 2, '.', '') * 100;
                  $totalDiscountCalculate += ($this->numberFormatprc((($indItemDiscount / $it->get('quantity')->value) / $no_of_payments), 2, '.', '') * $it->get('quantity')->value);
                }
                $unititemdiscount = $dom->createElement('UnitDiscount', $unit_Discount);
                $unititemdiscount = $orderitem->appendChild($unititemdiscount);
              }
              else {
                // If subscription.
                $unititemdiscount = $dom->createElement('UnitDiscount', $this->numberFormatprc($indItemDiscount / $it->get('quantity')->value, 2, '.', '') * 100);
                $unititemdiscount = $orderitem->appendChild($unititemdiscount);
                $totalDiscountCalculate += ($this->numberFormatprc($indItemDiscount / $it->get('quantity')->value, 2, '.', '') * $it->get('quantity')->value);
              }
            }
            else {
              $unititemdiscount = $dom->createElement('UnitDiscount', $this->numberFormatprc($indItemDiscount / $it->get('quantity')->value, 2, '.', '') * 100);
              $unititemdiscount = $orderitem->appendChild($unititemdiscount);
              $totalDiscountCalculate += ($this->numberFormatprc($indItemDiscount / $it->get('quantity')->value, 2, '.', '') * $it->get('quantity')->value);
            }
          }
          $unitD++;
        }
      }
      else {
        // If no adjustments.
        $totalTax = 0;
        $totalDiscount = 0;
        $grandTotalPrice += $this->numberFormatprc($this->order->getTotalPrice()->getNumber(), 2, '.', '');
      }
      $sku_code = $dom->createElement('SKU', '');
      $sku_code = $orderitem->appendChild($sku_code);
      $sku_code->appendChild($dom->createCDATASection($this->safeString($it->getPurchasedEntity()->sku->value, 100)));
    }
    // Calculate discounts at subtotal level or Discount at cart level.
    $totalItems = count($this->order->getItems());
    if (count($this->order->getAdjustments()) > 0) {
      foreach ($this->order->getAdjustments() as $adjustment) {
        if ($adjustment->getType() == 'shipping') {
          $shipmentType = $this->getShippingPluginType();
          if ($payment_details['fef_payment_options'] == 'Recurring') {
            if ($payment_details['fef_recuring_type_option'] == 'Installment') {
              // Installment.
              if ($payment_details['fef_cnp_noof_payments'] == 'Indefinite Recurring Only') {
                if ($shipmentType == 'flat_rate') {
                  $no_of_payments = $payment_details['fef_cnp_noof_payments'];
                  $shippingValueCalculate += ($this->numberFormatprc(($adjustment->getAmount()->getNumber() / 999), 2, '.', ''));
                }
                else {
                  $no_of_payments = $payment_details['fef_cnp_noof_payments'];
                  $shippingValueCalculate += ($this->numberFormatprc((($adjustment->getAmount()->getNumber() / $totalItems) / 999), 2, '.', '') * $totalItems);
                }
              }
              else {
                // For selected no.of payments.
                if ($shipmentType == 'flat_rate') {
                  $no_of_payments = $payment_details['fef_cnp_noof_payments'];
                  $shippingValueCalculate += ($this->numberFormatprc(($adjustment->getAmount()->getNumber() / $no_of_payments), 2, '.', ''));
                }
                else {
                  $no_of_payments = $payment_details['fef_cnp_noof_payments'];
                  $shippingValueCalculate += ($this->numberFormatprc((($adjustment->getAmount()->getNumber() / $totalItems) / $no_of_payments), 2, '.', '') * $totalItems);
                }
              }
            }
            else {
              // If subscription.
              $shippingValueCalculate += ($this->numberFormatprc($adjustment->getAmount()->getNumber(), 2, '.', ''));
            }
          }
          else {
            // If one time only.
            $shippingValueCalculate += ($this->numberFormatprc($adjustment->getAmount()->getNumber(), 2, '.', ''));
          }
        }
      }
    }

    if (!empty($shipment)) {
      if ($shipment->getAmount()->getNumber() != '') {
        // $shipment->get
        $shipping = $dom->createElement('Shipping', '');
        $shipping = $cporder->appendChild($shipping);
        $shipping_method = $dom->createElement('ShippingMethod', $this->safeString($this->getShippingPluginName(), 50));
        $shipping_method = $shipping->appendChild($shipping_method);
        $shipping_value = $dom->createElement('ShippingValue', $shippingValueCalculate * 100);
        $shipping_value = $shipping->appendChild($shipping_value);
      }
    }

    $receipt = $dom->createElement('Receipt', '');
    $receipt = $cporder->appendChild($receipt);

    if ($config->get('cnp.cnp_receipt_patron')[1] == 1) {
      $sendReceipt = 'true';
    }
    else {
      $sendReceipt = 'false';
    }
    $sendreceipt = $dom->createElement('SendReceipt', $sendReceipt);
    $sendreceipt = $receipt->appendChild($sendreceipt);
    $language = $dom->createElement('Language', 'ENG');
    $language = $receipt->appendChild($language);
    // Collect receipt t&c and header information.
    if ($config->get('cnp.cnp_receipt_header') != '') {
      $receiptHeader = $config->get('cnp.cnp_receipt_header');
    }
    else {
      $receiptHeader = '';
    }
    $orginfo = $dom->createElement('OrganizationInformation', '');
    $orginfo = $receipt->appendChild($orginfo);
    $orginfo->appendChild($dom->createCDATASection($receiptHeader));
    if ($config->get('cnp.cnp_terms_con') != '') {
      $receipttnc = $config->get('cnp.cnp_terms_con');
    }
    else {
      $receipttnc = "";
    }
    $termscon = $dom->createElement('TermsCondition', '');
    $termscon = $receipt->appendChild($termscon);
    $termscon->appendChild($dom->createCDATASection($receipttnc));
    $emailnotilist = $dom->createElement('EmailNotificationList', '');
    $emailnotilist = $receipt->appendChild($emailnotilist);
    $notificationemail = $dom->createElement('NotificationEmail', '');
    $notificationemail = $emailnotilist->appendChild($notificationemail);
    $transaction = $dom->createElement('Transaction', '');
    $transaction = $cporder->appendChild($transaction);
    if ($payment_details['fef_payment_methods'] == 'CreditCard') {
      if ($this->order->getTotalPrice()->getNumber() == 0) {

        $trans_type = $dom->createElement('TransactionType', 'PreAuthorization');
        $trans_type = $transaction->appendChild($trans_type);
      }
      else {
        $trans_type = $dom->createElement('TransactionType', 'Payment');
        $trans_type = $transaction->appendChild($trans_type);
      }
    }
    else {
      if ($this->order->getTotalPrice()->getNumber() == 0) {
        $trans_type = $dom->createElement('TransactionType', 'PreAuthorization');
        $trans_type = $transaction->appendChild($trans_type);
      }
      else {
        $trans_type = $dom->createElement('TransactionType', 'Payment');
        $trans_type = $transaction->appendChild($trans_type);
      }
    }
    $trans_desc = $dom->createElement('DynamicDescriptor', 'DynamicDescriptor');
    $trans_desc = $transaction->appendChild($trans_desc);
    if ($payment_details['fef_payment_options'] == 'Recurring') {
      $trans_recurr = $dom->createElement('Recurring', '');
      $trans_recurr = $transaction->appendChild($trans_recurr);
      if ($payment_details['fef_recuring_type_option'] == 'Installment') {
        // Installment.
        if ($payment_details['fef_cnp_noof_payments'] == 'Indefinite Recurring Only') {
          $no_of_payments = $payment_details['fef_cnp_noof_payments'];
          $total_installment = $dom->createElement('Installment', 998);
          $total_installment = $trans_recurr->appendChild($total_installment);
        }
        else {
          // For selected no.of payments.
          $no_of_payments = $payment_details['fef_cnp_noof_payments'];
          if ($no_of_payments != "") {
            $total_installment = $dom->createElement('Installment', $no_of_payments);
            $total_installment = $trans_recurr->appendChild($total_installment);
          }
          else {
            $total_installment = $dom->createElement('Installment', 1);
            $total_installment = $trans_recurr->appendChild($total_installment);
          }
        }
        $total_periodicity = $dom->createElement('Periodicity', $payment_details['fef_cnp_periodcity']);
        $total_periodicity = $trans_recurr->appendChild($total_periodicity);
        $recurringMethod = $dom->createElement('RecurringMethod', $payment_details['fef_recuring_type_option']);
        $recurringMethod = $trans_recurr->appendChild($recurringMethod);
      }
      else {
        if ($payment_details['fef_cnp_noof_payments'] == 'Indefinite Recurring Only') {
          $no_of_payments = $payment_details['fef_cnp_noof_payments'];
          $total_installment = $dom->createElement('Installment', 999);
          $total_installment = $trans_recurr->appendChild($total_installment);
        }
        else {
          // For selected no.of payments.
          $no_of_payments = $payment_details['fef_cnp_noof_payments'];
          if ($no_of_payments != "") {
            $total_installment = $dom->createElement('Installment', $no_of_payments);
            $total_installment = $trans_recurr->appendChild($total_installment);
          }
          else {
            $total_installment = $dom->createElement('Installment', 1);
            $total_installment = $trans_recurr->appendChild($total_installment);
          }
        }
        $total_periodicity = $dom->createElement('Periodicity', $payment_details['fef_cnp_periodcity']);
        $total_periodicity = $trans_recurr->appendChild($total_periodicity);
        $recurringMethod = $dom->createElement('RecurringMethod', "Subscription");
        $recurringMethod = $trans_recurr->appendChild($recurringMethod);
      }
    }
    $trans_totals = $dom->createElement('CurrentTotals', '');
    $trans_totals = $transaction->appendChild($trans_totals);

    if ($totalDiscountCalculate) {
      $total_discount = $dom->createElement('TotalDiscount', $totalDiscountCalculate * 100);
      $total_discount = $trans_totals->appendChild($total_discount);
    }
    if ($unitTaxCalculate) {
      $total_tax = $dom->createElement('TotalTax', $unitTaxCalculate * 100);
      $total_tax = $trans_totals->appendChild($total_tax);
    }
    // If ($shippingValueCalculate != '') {.
    if (!empty($shipment)) {
      $totalShipping = $dom->createElement('TotalShipping', $shippingValueCalculate * 100);
      $totalShipping = $trans_totals->appendChild($totalShipping);
    }
    $gtAmount = ($this->numberFormatprc($unitPriceCalculate, 2, '.', '') + $this->numberFormatprc($unitTaxCalculate, 2, '.', '') + $this->numberFormatprc($shippingValueCalculate, 2, '.', '')) - ($totalDiscountCalculate);
    if ($payment_details['fef_payment_options'] == 'Recurring') {
      if ($payment_details['fef_recuring_type_option'] == 'Installment') {
        // Installment.
        if ($payment_details['fef_cnp_noof_payments'] == 'Indefinite Recurring Only') {
          $no_of_payments = $payment_details['fef_cnp_noof_payments'];
          $total_amount = $dom->createElement('Total', ($this->numberFormatprc(($gtAmount), 2, '.', '') * 100));
          $total_amount = $trans_totals->appendChild($total_amount);
        }
        else {
          // For selected no.of payments.
          $no_of_payments = $payment_details['fef_cnp_noof_payments'];
          $total_amount = $dom->createElement('Total', ($this->numberFormatprc(($gtAmount), 2, '.', '') * 100));
          $total_amount = $trans_totals->appendChild($total_amount);
        }
      }
      else {
        // If subscription.
        $total_amount = $dom->createElement('Total', ($this->numberFormatprc($gtAmount, 2, '.', '') * 100));
        $total_amount = $trans_totals->appendChild($total_amount);
      }
    }
    else {
      // If one time only.
      $total_amount = $dom->createElement('Total', ($this->numberFormatprc($gtAmount, 2, '.', '') * 100));
      $total_amount = $trans_totals->appendChild($total_amount);
    }
    if (Order::load($this->order->id())->get('coupons')[0]) {
      $couponCode = Order::load($this->order->id())->get('coupons')[0]->entity->getCode();
      $trans_coupon = $dom->createElement('CouponCode', $couponCode);
      $trans_coupon = $transaction->appendChild($trans_coupon);
    }
    $strParam = $dom->saveXML();
    /*header('Content-type: text/xml');
    header('Content-Disposition: attachment; filename="text.xml"');
    echo $strParam;
    exit;*/
    if ($config->get('cnp.cnp_pre_auth') == 1 && $payment_details['fef_payment_options'] == 'Recurring') {
      if ($this->order->getTotalPrice()->getNumber() == 0) {
        try {
          if ($this->order->getTotalPrice()->getNumber() == 0 && $this->isShippingZero()) {
            throw new Exception('Zero Payment not allowed with Recurring.Please select One Time Only');
          }
        }
        catch (Exception $e) {
          $this->messenger()->addError($e->getMessage());
          $this->redirectToPreviousStep();
        }
      }
    }
    $connect    = [
      'soap_version' => 'SOAP_1_1',
      'trace' => 1,
      'exceptions' => 0,
    ];
    $client     = new \SoapClient('https://paas.cloud.clickandpledge.com/paymentservice.svc?wsdl', $connect);
    $soapParams = ['instruction' => $strParam];
    $response   = $client->Operation($soapParams);
    /*echo "<pre>";
    print_r($response);
    exit;*/
    if ($response->OperationResult->ResultCode == 0) {
      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      $payment = $payment_storage->create([
        'state' => 'authorization',
        'amount' => $this->order->getTotalPrice(),
        'payment_gateway' => $this->entityId,
        'order_id' => $this->order->id(),
        'test' => $this->getMode() == 'test',
        'remote_id' => $this->order->id(),
        'remote_state' => 'Success',
        'authorized' => $this->time->getRequestTime(),
      ]);
      $payment->save();
      $_SESSION['payment_details'] = '';
      $txID = $response->OperationResult->TransactionNumber;
      $this->messenger()->addMessage($this->t('Your payment was successful with Transaction id : @transaction_id', ['@transaction_id' => $txID]));
    }
    else {
      try {
        if ($response->OperationResult->ResultCode != 0) {
          $errorMsg = $response->OperationResult->ResultData . " " . $response->OperationResult->AdditionalInfo;
          // $errorMsg = $response->OperationResult->ResultData;
          throw new Exception($errorMsg . '. Please verify your details and try again.');
        }
      }
      catch (Exception $e) {
        // To delete payment method on failure transactions.
        // if(!stripos('Total amount is wrong',$e->getMessage()))
        // {
        $this->order->get("payment_gateway")->setValue(NULL);
        $this->order->get("payment_method")->setValue(NULL);
        if (!$payment_method->isReusable() || $payment_method->getOwner()->isAnonymous()) {
          $payment_method->delete();
        }
        // }
        // drupal_set_message($e->getMessage(), 'error');
        $this->messenger()->addError($e->getMessage());
        $this->redirectToPreviousStep();
      }

    }

  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $_SESSION['payment_details'] = $payment_details;
    $expires = CreditCard::calculateExpirationTimestamp(
        $payment_details['expiration']['month'], $payment_details['expiration']['year']
    );
    $payment_method->setExpiresTime($expires);
    // Set the payment method as not reusable.
    // @todo Allow configuring whether the payment methods should be reusable.
    $payment_method->setReusable(FALSE);
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $status = $request->get('status');
    // drupal_set_message();
    $this->messenger()->addError($this->t('Payment @status on @gateway but may resume the checkout process here when you are ready.', [
      '@status' => $status,
      '@gateway' => $this->getDisplayLabel(),
    ]));
  }

  /**
   * Create payment form.
   *
   * @param mixed $form
   *   An associative array containing the structure of the form.
   * @param mixed $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns the form structure
   */
  public function crateCnpConfigurationForm($form, $form_state) {
    $form['base_url_cnp'] = [
      '#type' => 'hidden',
      '#default_value' => base_path(),
      '#attributes' => ['id' => 'base_url_cnp'],
    ];
    return $form;
  }

  /**
   * To Sanitize the string by remove new line characters.
   *
   * @param mixed $str
   *   The string to be sanitize.
   * @param mixed $length
   *   Length of the string.
   * @param mixed $start
   *   String point to sanitize the string.
   *
   * @return mixed
   *   Returns a string by removing new line characters.
   */
  public function safeString($str, $length = 1, $start = 0) {
    // Remove new line characters.
    $str = preg_replace('/\x03/', '', $str);
    return substr(($str), $start, $length);
  }

  /**
   * To format the decimal number.
   *
   * @param mixed $number
   *   The number.
   * @param mixed $decimals
   *   No.of decimals.
   * @param mixed $decsep
   *   The decsep;.
   * @param mixed $ths_sep
   *   The ths_sep;.
   *
   * @return int
   *   Returns a formatted number.
   */
  public function numberFormatprc($number, $decimals = 2, $decsep = '', $ths_sep = '') {
    $parts = explode('.', $number);
    if (count($parts) > 1) {
      return $parts[0] . '.' . substr($parts[1], 0, $decimals);
    }
    else {
      return $number;
    }
  }

  /**
   * To redirect to previous page if any occur.
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   */
  protected function redirectToPreviousStep() {
    throw new NeedsRedirectException($_SERVER['HTTP_REFERER']);
  }

}
