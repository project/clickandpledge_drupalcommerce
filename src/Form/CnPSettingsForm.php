<?php

namespace Drupal\clickandpledge_drupalcommerce\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * The Account verification form.
 */
class CnPSettingsForm extends ConfigFormBase {
  /**
   * The Database connection.
   *
   * @var mixed
   */
  protected $connection;

  /**
   * The Module Handler.
   *
   * @var mixed
   */
  protected $moduleHandler;

  /**
   * The class Constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection object.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The Module Handler Object.
   */
  public function __construct(Connection $connection, ModuleHandlerInterface $module_handler) {
    $this->connection = $connection;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'), $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cnp.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cnp_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('cnp.settings');
    $form = $this->displayBasicForm($form, $config, $form_state);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('cnp.settings');
    if (!$this->isValidEmail(trim($form_state->getValue('cnp_vemail')))) {
      $form_state->setErrorByName('cnp_vemail', $this->t('Please enter valid Email.'));
    }
    if (!$config->get('cnp.cnp_vemail')) {
      if (empty($this->verifyEmailWithConnect($form_state->getValue('cnp_vemail')))) {
        $form_state->setErrorByName('cnp_vemail', $this->t('Please enter registered email with CONNECT.'));
      }
    }
    if (trim($form_state->getValue('cnp_verify_code')) != '') {
      $pat = '/^\d{5}$/';
      if (!preg_match($pat, trim($form_state->getValue('cnp_verify_code')))) {
        $form_state->setErrorByName('cnp_verify_code', $this->t('Enter valid verification code'));
      }
    }
    else {
      if ($config->get('cnp.cnp_vemail') != '') {
        $form_state->setErrorByName('cnp_verify_code', $this->t('Enter valid verification code'));
      }
    }

    if (trim($form_state->getValue('cnp_verify_code')) != '' && trim($form_state->getValue('cnp_vemail')) != '') {
      $data = $this->getCnpTransactions(trim($form_state->getValue('cnp_vemail')), trim($form_state->getValue('cnp_verify_code')));
      // print_r($data);
      // exit;
      if ($data == 'error') {
        $form_state->setErrorByName('cnp_verify_code', $this->t('Please enter valid verification code.'));
      }
    }

  }

  /**
   * Verify the email is valid or not.
   *
   * @param mixed $email
   *   The email user entered in the textbox.
   *
   * @return bool
   *   If valid email returns true or else returns false.
   */
  public function isValidEmail($email) {
    return preg_match('/^(([^<>()[\]\\.,;:\s@"\']+(\.[^<>()[\]\\.,;:\s@"\']+)*)|("[^"\']+"))@((\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\])|(([a-zA-Z\d\-]+\.)+[a-zA-Z]{2,}))$/', $email);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $cnp_verify_code = ($form_state->getValue('cnp_verify_code')) ? $form_state->getValue('cnp_verify_code') : '';
    $cnp_vemail = ($form_state->getValue('cnp_vemail')) ? $form_state->getValue('cnp_vemail') : '';

    $cnp_verify_code = trim($cnp_verify_code);
    $cnp_vemail = trim($cnp_vemail);

    if ($form_state->getValues('cnp_vemail') != '') {
      if (!$cnp_verify_code) {
        $vemail = $form_state->getValues()['cnp_vemail'];
        $curl = curl_init();
        $cnpemailaddress = $vemail;
        $response = curl_exec($curl);
        $this->config('cnp.settings')
          ->set('cnp.cnp_vemail', trim($form_state->getValue('cnp_vemail')))
          ->set('cnp.cnp_verify_code', trim($cnp_verify_code))
          ->save();
        $err = curl_error($curl);
        curl_close($curl);

      }
    }
    if ($cnp_verify_code != '' && $cnp_vemail != '') {
      $this->config('cnp.settings')
        ->set('cnp.cnp_vemail', trim($form_state->getValue('cnp_vemail')))
        ->set('cnp.cnp_verify_code', trim($cnp_verify_code))
        ->save();
      $this->cnpPageRedirect('cnpsettings');
    }

  }

  /**
   * Creates Account verification form.
   *
   * @param mixed $form
   *   An associative array containing the structure of the form.
   * @param mixed $config
   *   The configuration object.
   * @param mixed $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns the form structure.
   */
  public function displayBasicForm($form, $config, $form_state) {
    $query = $this->connection->query('SELECT cnpaccountsinfo_orgid FROM {dp_cnp_dp_jbcnpaccountsinfo}');
    $query->allowRowCount = TRUE;

    // Logo display.
    $modulePath = $this->moduleHandler->getModule('clickandpledge_drupalcommerce')->getPath();
    $cnpalogo = "<img src='" . base_path() . $modulePath . "/images/cnp_logo.png'>";
    $form['cnp_alogo'] = [
      '#prefix' => '<div class="cnp_dlogo"> ' . $cnpalogo,
      '#suffix' => '</div>',
    ];

    $form['heading_text_start'] = [
      '#markup' => '<div>
            			<p>Click & Pledge works by adding credit card fields on the checkout and then sending the details to Click & Pledge for verification.</p></div>',

    ];

    if ($query->rowCount() != 0) {
      $form['heading_text'] = [
        '#markup' => '<div><img src="" height="" width=""/>
                				<a class="button" href="cnpsettings">Go to settings</a></div><br><hr/>',
      ];
    }
    $form['cnp_vemail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter Email to verify'),
      '#description' => $this->t('Please Enter your Email to verify'),
      '#default_value' => $config->get('cnp.cnp_vemail'),
    ];

    if ($config->get('cnp.cnp_vemail') != '') {
      $form['cnp_verify_code'] = [
        '#type' => 'textfield',
        '#attributes' => ['id' => 'verifycode'],
        '#title' => $this->t('Enter Verify Code'),
        '#description' => $this->t('Please Enter your verification code'),
        '#default_value' => $config->get('cnp.cnp_verify_code'),
      ];

      $form['swdu_text'] = [
        '#markup' => '<div class="signin_with_diff_user">
                				<a class="" href="different_user_signin">Sign in with a different account</a></div>',
      ];

    }

    return $form;
  }

  /**
   * Page redirection.
   *
   * @param mixed $path
   *   The URL to redirect.
   */
  public function cnpPageRedirect($path) {
    $response = new RedirectResponse($path, 302);
    $response->send();
  }

  /**
   * The get the transactions.
   *
   * @param mixed $cnpemailid
   *   The email ID.
   * @param mixed $cnpcode
   *   The code.
   *
   * @return string
   *   Return success or error.
   */
  public function getCnpTransactions($cnpemailid, $cnpcode) {

    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_URL => 'https://aaas.cloud.clickandpledge.com/idserver/connect/token',
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => $this->getCnpwcTransactions($cnpemailid, $cnpcode),
      CURLOPT_HTTPHEADER => [
        'cache-control: no-cache',
        'content-type: application/x-www-form-urlencoded',
      ],
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    if ($err) {
      echo "cURL Error #:" . $err;
    }
    else {
      $cnptokendata = json_decode($response);

      if (!isset($cnptokendata->error)) {

        $cnptoken = (isset($cnptokendata->access_token)) ? $cnptokendata->access_token : '';
        $cnprtoken = (isset($cnptokendata->refresh_token)) ? $cnptokendata->refresh_token : '';
        $cnptransactios = $this->deleteCnpwcTransactions();
        $rtncnpdata = $this->insrtCnpwcTokenInfo($cnpemailid, $cnpcode, $cnptoken, $cnprtoken);

        if ($rtncnpdata != '') {
          $curl = curl_init();

          curl_setopt_array($curl, [
            CURLOPT_URL => 'https://api.cloud.clickandpledge.com/users/accountlist',
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
              'accept: application/json',
              "authorization: Bearer " . $cnptoken,
              'content-type: application/json',
            ],
          ]);

          $response = curl_exec($curl);
          $err = curl_error($curl);
          curl_close($curl);

          if ($err) {
            echo "cURL Error #:" . $err;
          }
          else {
            $cnpAccountsdata = json_decode($response);
            // print_r($cnpAccountsdata);
            if (is_object($cnpAccountsdata)) {
              if ($cnpAccountsdata->Message === 'Authorization has been denied for this request.') {
                return "error";
              }
            }
            else {
              // print_r($cnpAccountsdata->Message);exit;
              // if($cnpAccountsdata->Message == '$cnpAccountsdata')
              $cnptransactios = $this->deleteWccnpAccountsList();
              foreach ($cnpAccountsdata as $cnpkey => $cnpvalue) {
                $cnporgid = (isset($cnpvalue->OrganizationId)) ? $cnpvalue->OrganizationId : '';
                $cnporgname = (isset($cnpvalue->OrganizationName)) ? addslashes($cnpvalue->OrganizationName) : '';
                $cnpaccountid = (isset($cnpvalue->AccountGUID)) ? $cnpvalue->AccountGUID : '';
                $cnpufname = (isset($cnpvalue->UserFirstName)) ? addslashes($cnpvalue->UserFirstName) : '';
                $cnplname = (isset($cnpvalue->UserLastName)) ? addslashes($cnpvalue->UserLastName) : '';
                $cnpuid = (isset($cnpvalue->UserId)) ? $cnpvalue->UserId : '';
                $cnptransactios = $this->insertCnpwcAccountsInfo($cnporgid, $cnporgname, $cnpaccountid, $cnpufname, $cnplname, $cnpuid);
              }
              return 'success';
            }
          }
        }
      }
      else {
        return 'error';
      }

    }
  }

  /**
   * To get the settings info.
   *
   * @param mixed $cnpemailid
   *   The user email.
   * @param mixed $cnpcode
   *   The user code.
   *
   * @return string
   *   Returns a string with account information.
   */
  public function getCnpwcTransactions($cnpemailid, $cnpcode) {
    $query = $this->connection->query('SELECT * FROM {dp_cnp_dp_jbcnpsettingsinfo}');
    $results = $query->fetchAssoc();

    $count = count($results);
    for ($i = 0; $i < $count; $i++) {
      $password = 'password';
      $cnpsecret = openssl_decrypt($results['cnpsettingsinfo_clentsecret'], "AES-128-ECB", $password);
      $rtncnpdata = "client_id=" . $results['cnpsettingsinfo_clientid'] . "&client_secret=" . $cnpsecret . "&grant_type=" . $results['cnpsettingsinfo_granttype'] . "&scope=" . $results['cnpsettingsinfo_scope'] . "&username=" . $cnpemailid . "&password=" . $cnpcode;
    }

    return $rtncnpdata;
  }

  /**
   * Deletes the token informations.
   */
  public function deleteCnpwcTransactions() {
    $this->connection->delete('dp_cnp_dp_jbcnptokeninfo')->execute();
  }

  /**
   * To save the token information related to the account.
   *
   * @param mixed $cnpemailid
   *   The User Email.
   * @param mixed $cnpcode
   *   The user code.
   * @param mixed $cnptoken
   *   The access Token.
   * @param mixed $cnprtoken
   *   The refresh token.
   *
   * @return int
   *   Returns inserted id.
   */
  public function insrtCnpwcTokenInfo($cnpemailid, $cnpcode, $cnptoken, $cnprtoken) {
    $fields = [
      'cnptokeninfo_username' => $cnpemailid,
      'cnptokeninfo_code' => $cnpcode,
      'cnptokeninfo_accesstoken' => $cnptoken,
      'cnptokeninfo_refreshtoken' => $cnprtoken,
    ];
    $this->connection->insert('dp_cnp_dp_jbcnptokeninfo')->fields($fields)->execute();
    $res = $this->connection->query("select max(cnptokeninfo_id) from {dp_cnp_dp_jbcnptokeninfo}");
    $id = $res->fetchCol();
    return $id[0];
  }

  /**
   * To delete the account information.
   */
  public function deleteWccnpAccountsList() {
    $this->connection->delete('dp_cnp_dp_jbcnpaccountsinfo')->execute();
  }

  /**
   * To save accounts information.
   *
   * @param mixed $cnporgid
   *   The organization id.
   * @param mixed $cnporgname
   *   The organization name.
   * @param mixed $cnpaccountid
   *   The account id.
   * @param mixed $cnpufname
   *   User First Name.
   * @param mixed $cnplname
   *   Users Last Name.
   * @param mixed $cnpuid
   *   User Id.
   *
   * @return Number
   *   Returns inserted ID.
   */
  public function insertCnpwcAccountsInfo($cnporgid, $cnporgname, $cnpaccountid, $cnpufname, $cnplname, $cnpuid) {
    $this->connection->insert('dp_cnp_dp_jbcnpaccountsinfo')
      ->fields([
        'cnpaccountsinfo_orgid' => $cnporgid,
        'cnpaccountsinfo_orgname' => $cnporgname,
        'cnpaccountsinfo_accountguid' => $cnpaccountid,
        'cnpaccountsinfo_userfirstname' => $cnpufname,
        'cnpaccountsinfo_userlastname' => $cnplname,
        'cnpaccountsinfo_userid' => $cnpuid,
      ])
      ->execute();
    $res = $this->connection->query("select max(cnpaccountsinfo_id) from {dp_cnp_dp_jbcnpaccountsinfo}");
    $id = $res->fetchCol();
    return $id[0];
  }

  /**
   * Verify the email is exists or not with connect.
   *
   * @param mixed $email
   *   The use email to verify.
   *
   * @return Object
   *   Returns the response as an object.
   */
  public function verifyEmailWithConnect($email) {
    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_URL => "https://api.cloud.clickandpledge.com/users/requestcode",
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => [
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded",
        "email: " . $email,
      ],
    ]);
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
      return "cURL Error #:" . $err;
    }
    else {
      return $response;
    }
  }

}
