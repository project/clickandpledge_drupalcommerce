<?php

namespace Drupal\clickandpledge_drupalcommerce\Form;

use SimpleXMLElement;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * This class handles Payment settings form.
 */
class CnPSettings extends ConfigFormBase {

  /**
   * The Module Handler.
   *
   * @var mixed
   */
  protected $moduleHandler;

  /**
   * Stores the configuration for the current module.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configModule;

  /**
   * The Database connection.
   *
   * @var mixed
   */
  protected $connection;

  /**
   * The class Constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The Module handler.
   * @param \Drupal\Core\Database\Connection $connection
   *   The Database connection.
   */
  public function __construct(ModuleHandlerInterface $module_handler, Connection $connection) {
    $this->moduleHandler = $module_handler;
    $this->connection = $connection;
    $this->configModule = $this->config('cnp.mainsettings');
    $query = $this->connection->query('SELECT 	cnpaccountsinfo_id FROM {dp_cnp_dp_jbcnpaccountsinfo}');
    $query->allowRowCount = TRUE;
    if ($query->rowCount() === 0) {
      $this->cnpPageRedirect('cnpauth');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'), $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cnp.mainsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cnp_main_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = $this->displaySettingsForm($form, $this->configModule, $form_state);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Page Redirection.
   *
   * @param mixed $path
   *   The URL to redirect a page.
   */
  public function cnpPageRedirect($path) {
    $response = new RedirectResponse($path, 302);
    $response->send();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('cnp.mainsettings')
      ->set('cnp.cnp_status_enable', $form_state->getValue('cnp_status_enable'))
      ->set('cnp.cnp_title', trim($form_state->getValue('cnp_title')))
      ->set('cnp.cnp_desc', trim($form_state->getValue('cnp_desc')))
      ->set('cnp.cnp_accid', $form_state->getValue('cnp_accid'))
      ->set('cnp.cnp_camp_urls', $form_state->getValue('cnp_camp_urls'))
      ->set('cnp.cnp_mode', $form_state->getValue('cnp_mode'))
      ->set('cnp.cnp_payment_credit_cards', $form_state->getValue('cnp_payment_credit_cards'))
      ->set('cnp.cnp_payment_credit_card_options_hidden', $form_state->getValue('cnp_payment_credit_card_options_hidden'))
      ->set('cnp.cnp_pre_auth', $form_state->getValue('cnp_pre_auth'))
      ->set('cnp.cnp_receipt_patron', $form_state->getValue('cnp_receipt_patron'))
      ->set('cnp.cnp_receipt_header', trim($form_state->getValue('cnp_receipt_header')))
      ->set('cnp.cnp_terms_con', trim($form_state->getValue('cnp_terms_con')))
      ->set('cnp.cnp_recurr_label', trim($form_state->getValue('cnp_recurr_label')))
      ->set('cnp.cnp_recurr_settings', trim($form_state->getValue('cnp_recurr_settings')))
      ->set('cnp.cnp_recurr_oto', $form_state->getValue('cnp_recurr_oto'))
      ->set('cnp.cnp_recurr_recur', $form_state->getValue('cnp_recurr_recur'))
      ->set('cnp.cnp_default_payment_options', $form_state->getValue('cnp_default_payment_options'))
      ->set('cnp.cnp_recurring_types', trim($form_state->getValue('cnp_recurring_types')))
      ->set('cnp.cnp_recurr_type_option', $form_state->getValue('cnp_recurr_type_option'))
      ->set('cnp.cnp_default_recurring_type', $form_state->getValue('cnp_default_recurring_type'))
      ->set('cnp.cnp_recurring_periodicity', trim($form_state->getValue('cnp_recurring_periodicity')))
      ->set('cnp.cnp_recurring_periodicity_options', $form_state->getValue('cnp_recurring_periodicity_options'))
      ->set('cnp.cnp_recurring_no_of_payments', trim($form_state->getValue('cnp_recurring_no_of_payments')))
      ->set('cnp.cnp_recurring_no_of_payments_options', $form_state->getValue('cnp_recurring_no_of_payments_options'))
      ->set('cnp.cnp_recurring_default_no_payment_lbl', trim($form_state->getValue('cnp_recurring_default_no_payment_lbl')))
      ->set('cnp.cnp_recurring_default_no_payments', (int) trim($form_state->getValue('cnp_recurring_default_no_payments')))
      ->set('cnp.cnp_recurring_max_no_payment_lbl', trim($form_state->getValue('cnp_recurring_max_no_payment_lbl')))
      ->set('cnp.cnp_recurring_max_no_payment', (int) trim($form_state->getValue('cnp_recurring_max_no_payment')))
      ->set('cnp.cnp_recurring_default_no_payment_open_field_lbl', trim($form_state->getValue('cnp_recurring_default_no_payment_open_field_lbl')))
      ->set('cnp.cnp_recurring_default_no_payments_open_filed', (int) trim($form_state->getValue('cnp_recurring_default_no_payments_open_filed')))
      ->set('cnp.cnp_recurring_max_no_payment_open_filed_lbl', trim($form_state->getValue('cnp_recurring_max_no_payment_open_filed_lbl')))
      ->set('cnp.cnp_recurring_max_no_payment_open_filed', (int) trim($form_state->getValue('cnp_recurring_max_no_payment_open_filed')))
      ->set('cnp.cnp_recurring_default_no_payment_fncc_lbl', trim($form_state->getValue('cnp_recurring_default_no_payment_fncc_lbl')))
      ->set('cnp.cnp_recurring_default_no_payments_fnnc', (int) trim($form_state->getValue('cnp_recurring_default_no_payments_fnnc')))
      ->save();
  }

  /**
   * Creates a Payment settings form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param object $config
   *   The configuration object.
   * @param mixed $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns The form structure.
   */
  public function displaySettingsForm(array $form, $config, $form_state) {
    $query = $this->connection->query('SELECT cnptokeninfo_username FROM {dp_cnp_dp_jbcnptokeninfo}');
    $result = $query->fetchAll();
    $loggedinAs = $result[0]->cnptokeninfo_username;

    $form['cnp_bform_main_div_start'] = [
      '#prefix' => '<div class="cnp_bform_main_div">',
    ];

    // Logo display.
    $modulePath = $this->moduleHandler->getModule('clickandpledge_drupalcommerce')->getPath();
    $cnpdlogo = "<img src='" . base_path() . $modulePath . "/images/cnp_logo.png'>";
    $form['cnp_dlogo'] = [
      '#prefix' => '<div class="cnp_dlogo"> ' . $cnpdlogo,
      '#suffix' => '</div>',
    ];

    $form['heading_text'] = [
      '#markup' => '<div>
			<p>Click & Pledge works by adding credit card fields on the checkout and then sending the details to Click & Pledge for verification.</p>
                                                      			<p>You are logged in as <b>[ ' . $loggedinAs . ' ]</b></p>
                                                      			<a class="button" href="cnpauth">Change User</a></div><br><hr/>',
    ];
    $form['base_url_cnp'] = [
      '#type' => 'hidden',
      '#default_value' => base_path(),
      '#attributes' => ['id' => 'base_url_cnp'],
    ];

    $form['cnp_status_enable'] = [
      '#type' => 'checkboxes',
      '#title' => 'Status',
      '#options' => ['yes' => $this->t('Enable Click & Pledge')],
      '#default_value' => ($config->get('cnp.cnp_status_enable')) ? $config->get('cnp.cnp_status_enable') : [],
      '#prefix' => '<div class="container-inline cnp_bform_status_check">',
      '#suffix' => '</div>',
    ];

    $form['cnp_title'] = [
      '#type' => 'textfield',
      '#attributes' => ['' => 'myclass'],
      '#title' => $this->t('Title'),
      '#default_value' => $config->get('cnp.cnp_title'),
      '#required' => TRUE,
      '#prefix' => '<div class="cnp_bform_title">',
      '#suffix' => '</div>',
    ];
    $form['cnp_desc'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#default_value' => $config->get('cnp.cnp_desc'),
      '#prefix' => '<div class="cnp_bform_desc">',
      '#suffix' => '</div>',
    ];

    $form['cnp_accid'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => $this->t('C&P Account ID'),
      '#default_value' => $config->get('cnp.cnp_accid'),
      '#prefix' => '<div id="cnp_accid_wrapper" class="cnp_bform_accid">',
      '#suffix' => '</div>',
      '#options' => $this->getAccountIds(),
      '#ajax' => [
        'callback' => [$this, 'changeOptionsAjax'],
        'wrapper' => 'cnp_camp_urls_wrapper',
      ],
    ];
    $form['cnp_accid_hidden'] = [
      '#type' => 'hidden',
      '#attributes' => ['id' => 'cnp_accid_hidden'],
      '#default_value' => $config->get('cnp.cnp_accid'),
    ];

    $form['refresh_accounts'] = [
      '#markup' => '<a href="#" id="rfrshtokens">' . $this->t('Refresh Accounts') . '</a>',
    ];
    // Onload display campaign url of the selected account ID.
    $form['cnp_camp_urls'] = [
      '#type' => 'select',
      '#title' => $this->t('CONNECT Campaign URL Alias'),
      '#default_value' => $config->get('cnp.cnp_camp_urls'),
      '#options' => $this->getOptions($form_state, $config),
      '#prefix' => '<div id="cnp_camp_urls_wrapper" class="cnp_bform_camp_urls">',
      '#suffix' => '</div>',
    ];
    $form['cnp_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Mode'),
      '#options' => [
        'Yes' => $this->t('Test'),
        'No' => $this->t('Live'),
      ],
      '#default_value' => ($config->get('cnp.cnp_mode') !== NULL) ? $config->get('cnp.cnp_mode') : 'Yes',
      '#prefix' => '<div class="container-inline cnp_bform_mode">',
      '#suffix' => '</div>',
    ];

    // PAYMENT METHODS.
    $form['cnp_payment_credit_cards'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Payment Methods'),
      '#attributes' => ['checked' => 'checked', 'disabled' => 'disabled'],
      '#options' => ['Credit Cards' => 'Credit Cards'],
      '#default_value' => ($config->get('cnp.cnp_payment_credit_cards')) ? $config->get('cnp.cnp_payment_credit_cards') : [],
      '#prefix' => '<div class="container-inline cnp_bform_payment_cards">',
      '#suffix' => '</div>',
    ];
    // Get the payment options once the page has loaded.
    if ($config->get('cnp.cnp_accid')) {
      $serviceOPtions = $this->getCnpActivePaymentList($config->get('cnp.cnp_accid'));
      $sOpt = ['Amex', 'Discover', 'Jcb', 'Master', 'Visa'];
      if (!empty($serviceOPtions)) {
        $originalOptions = [];
        $payOptions = $serviceOPtions->GetAccountDetailResult;
        foreach ($payOptions as $k => $v) {
          if (in_array($k, $sOpt)) {
            if ($v === 1) {
              $originalOptions[$k] = $k;
            }
          }
        }
      }
      else {
        $originalOptions = ['Amex', 'Discover', 'Jcb', 'Master', 'Visa'];
      }
    }
    else {
      $originalOptions = ['Amex', 'Discover', 'Jcb', 'Master', 'Visa'];
    }
    $form['cnp_payment_credit_card_options'] = [
      '#prefix' => '<div class="container-inline cnp_payment_options_wrapper" id="payment_options_wrapper">',
      '#suffix' => '</div>',
    ];
    $cards_hidden = '';
    foreach ($originalOptions as $op) {
      $cards_hidden .= $op . "#";
    }
    $form['cnp_payment_credit_card_options_hidden'] = [
      '#type' => 'hidden',
      '#attributes' => ['id' => 'card_options_hidden'],
      '#default_value' => $cards_hidden,
      '#prefix' => '<div class="" id="credit_card_options_hidden">',
      '#suffix' => '</div>',
    ];

    $form['note_text'] = [
      '#markup' => '<div>
                                                      			<p><b>Note:</b> Due to limitations with the Drupal Commerce payment API, only credit card payment is currently supported.</p>',
    ];

    $form['cnp_pre_auth'] = [
      '#type' => 'hidden',
      '#default_value' => 1,
    ];

    $form['receipt_settings_start'] = [
      '#prefix' => '<div class="cnp_bform_receipt_settings">',
    ];

    $form['some_text'] = [
      '#markup' => '<H2>' . $this->t('Receipt Settings') . '</H2>',
    ];
    $form['cnp_receipt_patron'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Send Receipt to Patron'),
      '#options' => ['1' => 'Send Receipt to Patron'],
      '#default_value' => ($config->get('cnp.cnp_receipt_patron')) ? $config->get('cnp.cnp_receipt_patron') : [],
      '#prefix' => '<div class="cnp_bform_receipt_patron">',
      '#suffix' => '</div>',
    ];
    $form['cnp_receipt_header'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Receipt Header'),
      '#attributes' => ['maxlength' => '1500', 'id' => 'cnp_receipt_head_msg'],
      '#default_value' => $config->get('cnp.cnp_receipt_header'),
      '#prefix' => '<div class="cnp_bform_receipt_header">',
      '#suffix' => '</div>',
      '#description' => "Maximum: 1500 characters, the following HTML tags are allowed: " . htmlspecialchars("<P></P><BR /><OL></OL><LI></LI><UL></UL>") . ". You have <span id='cnpheadcount'>1500</span> characters left.",
    ];
    $form['cnp_terms_con'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Terms & Conditions'),
      '#attributes' => ['maxlength' => '1500', 'id' => 'cnp_terms_con_msg'],
      '#default_value' => $config->get('cnp.cnp_terms_con'),
      '#prefix' => '<div class="cnp_bform_terms_con">',
      '#suffix' => '</div>',
      '#description' => "To be added at the bottom of the receipt. Typically the text provides proof that the patron has read & agreed to the terms & conditions. The following HTML tags are allowed: " . htmlspecialchars("<P></P><BR /><OL></OL><LI></LI><UL></UL>") . ". 
                                                      Maximum: 1500 characters, You have <span id='cnptnccount'>1500</span> characters left.",
    ];
    $form['receipt_settings_end'] = [
      '#suffix' => '</div>',
    ];

    $form['some_text1'] = [
      '#markup' => '<H2 class="recurring_set">' . $this->t('Recurring Settings') . '</H2>',
    ];
    $form['cnp_recurr_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => ($config->get('cnp.cnp_recurr_label')) ? $config->get('cnp.cnp_recurr_label') : "Set this as a recurring payment",
      '#prefix' => '<div class="cnp_bform_recurr_label">',
      '#suffix' => '</div>',
    ];
    $form['cnp_recurr_settings'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Settings'),
      '#default_value' => ($config->get('cnp.cnp_recurr_settings')) ? $config->get('cnp.cnp_recurr_settings') : "Payment options",
      '#prefix' => '<div class="cnp_bform_recurr_settings">',
      '#suffix' => '</div>',
    ];
    $form['cnp_recurr_oto'] = [
      '#type' => 'checkboxes',
      '#options' => ['oto' => $this->t('One Time Only')],
      '#default_value' => ($config->get('cnp.cnp_recurr_oto')) ? $config->get('cnp.cnp_recurr_oto') : [],
      '#prefix' => '<div class="cnp_recurr_oto">',
      '#suffix' => '</div>',
    ];
    $form['cnp_recurr_recur'] = [
      '#type' => 'checkboxes',
      '#options' => ['1' => $this->t('Recurring')],
      '#default_value' => ($config->get('cnp.cnp_recurr_recur')) ? $config->get('cnp.cnp_recurr_recur') : [],
      '#prefix' => '<div class="cnp_recurr_recur">',
      '#suffix' => '</div>',
    ];

    $form['recurr_option_div_start'] = [
      '#markup' => '<div class="recurr_option">',
    ];

    $form['cnp_default_payment_options'] = [
      '#type' => 'select',
      '#title' => $this->t('Default Payment Options'),
      '#options' => [
        'Recurring' => $this->t('Recurring'),
        'One Time Only' => $this->t('One Time Only'),
      ],
      '#prefix' => '<div class="container-inline cnp_bform_default_payment_options" id="default_payment_options_wrapper">',
      '#suffix' => '</div>',
      '#default_value' => $config->get('cnp.cnp_default_payment_options'),
    ];
    $form['cnp_recurring_types'] = [
      '#type' => 'textfield',
      '#title' => '',
      '#prefix' => '<div class="cnp_bform_recurring_types">',
      '#suffix' => '</div>',
      '#default_value' => ($config->get('cnp.cnp_recurring_types')) ? $config->get('cnp.cnp_recurring_types') : "Recurring types",
    ];
    $form['cnp_recurr_type_option'] = [
      '#type' => 'checkboxes',
      '#options' => [
        'Installment' => $this->t('Installment (e.g. pay $1000 in 10 installments of $100 each)'),
        'Subscription' => $this->t('Subscription (e.g. pay $100 every month for 12 months)'),
      ],
      '#default_value' => ($config->get('cnp.cnp_recurr_type_option')) ? $config->get('cnp.cnp_recurr_type_option') : ["Installment" => "Installment (e.g. pay $1000 in 10 installments of $100 each)"],
      '#prefix' => '<div class="cnp_bform_recurr_type_option">',
      '#suffix' => '</div>',
    ];
    $form['cnp_default_recurring_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Default Recurring type'),
      '#options' => [
        'Subscription' => $this->t('Subscription'),
        'Installment' => $this->t('Installment'),
      ],
      '#prefix' => '<div class="container-inline cnp_bform_default_recurring_type" id="default_recurring_type_wrapper">',
      '#suffix' => '</div>',
      '#default_value' => $config->get('cnp.cnp_default_recurring_type'),
    ];
    $form['cnp_recurring_periodicity'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="cnp_bform_recurring_periodicity">',
      '#suffix' => '</div>',
      '#default_value' => ($config->get('cnp.cnp_recurring_periodicity')) ? $config->get('cnp.cnp_recurring_periodicity') : "Periodicity",
    ];
    $form['cnp_recurring_periodicity_options'] = [
      '#type' => 'checkboxes',
      '#options' => [
        'Week' => $this->t('Week'),
        '2 Weeks' => $this->t('2 Weeks'),
        'Month' => $this->t('Month'),
        '2 Months' => $this->t('2 Months'),
        'Quarter' => $this->t('Quarter'),
        '6 Months' => $this->t('6 Months'),
        'Year' => $this->t('Year'),
      ],
      '#default_value' => ($config->get('cnp.cnp_recurring_periodicity_options')) ? $config->get('cnp.cnp_recurring_periodicity_options') : [],
      '#prefix' => '<div class="cnp_bform_recurring_periodicity_options">',
      '#suffix' => '</div>',
    ];
    $form['cnp_recurring_no_of_payments'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="cnp_bform_recurring_no_of_payments">',
      '#default_value' => ($config->get('cnp.cnp_recurring_no_of_payments')) ? $config->get('cnp.cnp_recurring_no_of_payments') : "Number of payments",
    ];
    $form['cnp_recurring_no_of_payments_options'] = [
      '#type' => 'radios',
      '#options' => [
        'indefinite_openfield' => $this->t('Indefinite + Open Field Option'),
        '1' => $this->t('Indefinite Only'),
        'openfield' => $this->t('Open Field Only'),
        'fixednumber' => $this->t('Fixed Number - No Change Allowed'),
      ],
      '#suffix' => '</div>',
      '#default_value' => $config->get('cnp.cnp_recurring_no_of_payments_options'),
    ];
    $form['cnp_recurring_default_no_payment_lbl'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="default_no_of_payments_wrapper_start"><div class="container-inline cnp_bform_no_payment_lbl">',
      '#default_value' => ($config->get('cnp.cnp_recurring_default_no_payment_lbl')) ? $config->get('cnp.cnp_recurring_default_no_payment_lbl') : "Default number of payments",
    ];
    $form['cnp_recurring_default_no_payments'] = [
      '#type' => 'textfield',
      '#attributes' => ['maxlength' => 3],
      '#suffix' => '</div>',
      '#default_value' => ($config->get('cnp.cnp_recurring_default_no_payments')) ? $config->get('cnp.cnp_recurring_default_no_payments') : "",
    ];
    $form['cnp_recurring_max_no_payment_open_filed_lbl'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="container-inline cnp_bform_fix_number_no_change" id="fix-number-no-change">',
      '#default_value' => ($config->get('cnp.cnp_recurring_max_no_payment_open_filed_lbl')) ? $config->get('cnp.cnp_recurring_max_no_payment_open_filed_lbl') : "Maximum number of installments allowed",
    ];
    $form['cnp_recurring_max_no_payment'] = [
      '#type' => 'textfield',
      '#attributes' => ['maxlength' => 3],
      '#suffix' => '</div></div>',
      '#default_value' => ($config->get('cnp.cnp_recurring_max_no_payment')) ? $config->get('cnp.cnp_recurring_max_no_payment') : "",
    ];
    // Open filed form.
    $form['cnp_recurring_default_no_payment_open_field_lbl'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="open_filed_wrapper_start"><div class="container-inline cnp_bform_open_field_lbl">',
      '#default_value' => ($config->get('cnp.cnp_recurring_default_no_payment_open_field_lbl')) ? $config->get('cnp.cnp_recurring_default_no_payment_open_field_lbl') : "Default number of payments",
    ];

    $form['cnp_recurring_default_no_payments_open_filed'] = [
      '#type' => 'textfield',
      '#attributes' => ['maxlength' => 3],
      '#default_value' => ($config->get('cnp.cnp_recurring_default_no_payments_open_filed')) ? $config->get('cnp.cnp_recurring_default_no_payments_open_filed') : "",
      '#suffix' => '</div>',
    ];
    $form['cnp_recurring_max_no_payment_lbl'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="container-inline cnp_bform_max_no_payment" id="">',
      '#default_value' => ($config->get('cnp.cnp_recurring_max_no_payment_lbl')) ? $config->get('cnp.cnp_recurring_max_no_payment_lbl') : "Maximum number of installments allowed",
    ];

    $form['cnp_recurring_max_no_payment_open_filed'] = [
      '#type' => 'textfield',
      '#attributes' => ['disabled' => 'disabled', 'maxlength' => 3],
      '#suffix' => '</div></div>',
      '#default_value' => ($config->get('cnp.cnp_recurring_max_no_payment_open_filed')) ? $config->get('cnp.cnp_recurring_max_no_payment_open_filed') : "999",
    ];
    // Fixed Number nochange allowed filed.
    $form['cnp_recurring_default_no_payment_fncc_lbl'] = [
      '#type' => 'textfield',
      '#prefix' => '<div class="fixed_number_no_chnage_wrapper_start"><div class="container-inline cnp_bform_no_change">',
      '#default_value' => ($config->get('cnp.cnp_recurring_default_no_payment_fncc_lbl')) ? $config->get('cnp.cnp_recurring_default_no_payment_fncc_lbl') : "Default number of payments",
    ];
    $form['cnp_recurring_default_no_payments_fnnc'] = [
      '#type' => 'textfield',
      '#attributes' => ['maxlength' => 3],
      '#suffix' => '</div></div>',
      '#default_value' => ($config->get('cnp.cnp_recurring_default_no_payments_fnnc')) ? $config->get('cnp.cnp_recurring_default_no_payments_fnnc') : "",
    ];

    $form['recurr_option_div_end'] = [
      '#markup' => '</div>',
    ];

    $form['cnp_bform_main_div_end'] = [
      '#suffix' => '</div>',
    ];

    return $form;
  }

  /**
   * Get All accound iD's.
   *
   * @return array
   *   This method returns an array of options.
   */
  public function getAccountIds() {
    $query = $this->connection->query('SELECT cnpaccountsinfo_orgid, cnpaccountsinfo_orgname FROM {dp_cnp_dp_jbcnpaccountsinfo}');
    $result = $query->fetchAll();
    $opt = [];
    $opt[''] = '-select-';
    foreach ($result as $acc) {
      $opt[$acc->cnpaccountsinfo_orgid] = $acc->cnpaccountsinfo_orgid . " [" . $acc->cnpaccountsinfo_orgname . "]";
    }
    return $opt;
  }

  /**
   * The AJAX Callback.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns The form structure.
   */
  public function changeOptionsAjax(array &$form, FormStateInterface $form_state) {
    return $form['cnp_camp_urls'];
  }

  /**
   * The Refresh account action to get fresh and updated data.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns The form structure.
   */
  public function refreshAccounts(array &$form, FormStateInterface $form_state) {
    return $form['cnp_accid'];
  }

  /**
   * The AJAX Callback function.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param object $config
   *   The configuration object.
   *
   * @return string
   *   Returns account options.
   */
  public function getOptions(FormStateInterface $form_state, $config) {
    $camrtrnval[''] = [];
    if ($form_state->getValue('cnp_accid') == '') {

      $camrtrnval[''] = $this->t('--Select Campaign Name--');
      $accid = $config->get('cnp.cnp_accid');
      $opt = $this->getCnpUserEmailAccountList($accid)[0];
      if (!empty($opt)) {
        if (is_array($opt)) {
          for ($in = 0; $in < count($opt); $in++) {
            $camrtrnval[$opt[$in]->alias] = $opt[$in]->name . " (" . $opt[$in]->alias . ")";
          }
        }
        else {
          $camrtrnval[$opt->alias] = $opt->name . " (" . $opt->alias . ")";
        }
      }
      else {
        $camrtrnval[''] = $this->t('Campaign URL Alias are not found');
      }

      if ($config->get('cnp.cnp_camp_urls') !== '') {

      }
      else {
        $camrtrnval[''] = $this->t('--Select Campaign Name--');
      }
    }
    else {
      $optionData = $this->getCnpUserEmailAccountList($form_state->getValue('cnp_accid'));

      $camrtrnval[''] = $this->t('--Select Campaign Name--');
      $cnpsel = '';

      $camrtrnval[''] = $this->t('--Select Campaign Name--');
      $options = $optionData[0];

      if (count($options) === 1) {
        $camrtrnval[$options->alias] = $options->name . " (" . $options->alias . ")";
      }
      else {
        for ($inc = 0; $inc < count($options); $inc++) {
          $camrtrnval[$options[$inc]->alias] = $options[$inc]->name . " (" . $options[$inc]->alias . ")";
        }
      }
    }

    natcasesort($camrtrnval);
    return $camrtrnval;
  }

  /**
   * To get the accounts list.
   *
   * @param mixed $cnpacid
   *   The account ID.
   *
   * @return array
   *   Returns an array of options.
   */
  public function getCnpUserEmailAccountList($cnpacid) {
    $cnpwcaccountid = $cnpacid;
    $totRes = [];

    $cnprtrntxt = $this->getwcCnpConnectCampaigns($cnpwcaccountid);
    $totRes[] = $cnprtrntxt;
    $clist = [];
    $cnprtrnpaymentstxt = $this->getCnpActivePaymentList($cnpwcaccountid);
    if (count($cnprtrnpaymentstxt) > 0) {
      foreach ($cnprtrnpaymentstxt as $obj => $cli) {
        foreach ($cli as $key => $value) {
          if ($value === 1) {
            $clist[$key] = $key;
          }
        }
      }
    }
    else {
      $clist = [];
    }
    $totRes[] = $clist;
    return $totRes;
  }

  /**
   * To get Connect campaigns based on accountid.
   *
   * @param mixed $cnpaccid
   *   The account ID.
   *
   * @return array
   *   Returns connect campaigns array.
   */
  public function getwcCnpConnectCampaigns($cnpaccid) {

    $cnpacountid = $cnpaccid;
    $cnpaccountGUID = $this->getwcCnpAccountGuid($cnpacountid);
    $cnpUID = '14059359-D8E8-41C3-B628-E7E030537905';
    $cnpKey = '5DC1B75A-7EFA-4C01-BDCD-E02C536313A3';
    $connect = ['soap_version' => 'SOAP_1_1', 'trace' => 1, 'exceptions' => 0];
    $default = [
      'trace' => 1,
      'exceptions' => TRUE,
      'cache_wsdl' => WSDL_CACHE_NONE,
      'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
    ];
    $client = new \SoapClient('https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl', $connect);
    if (isset($cnpacountid) && $cnpacountid !== '' && isset($cnpaccountGUID) && $cnpaccountGUID !== '') {
      $xmlr = new SimpleXMLElement('<GetActiveCampaignList2></GetActiveCampaignList2>');

      $cnpsel = '';
      $xmlr->addChild('accountId', $cnpacountid);
      $xmlr->addChild('AccountGUID', $cnpaccountGUID);
      $xmlr->addChild('username', $cnpUID);
      $xmlr->addChild('password', $cnpKey);

      $response = $client->GetActiveCampaignList2($xmlr);
      $responsearr = $response->GetActiveCampaignList2Result->connectCampaign;
    }
    else {
      $responsearr = [];
    }
    return $responsearr;
  }

  /**
   * To get active or supported payment options.
   *
   * @param mixed $cnpaccid
   *   The account ID.
   *
   * @return mixed
   *   Returns payment options array.
   */
  public function getCnpActivePaymentList($cnpaccid) {
    $cnpacountid = $cnpaccid;
    $cnpaccountGUID = $this->getwcCnpAccountGuid($cnpacountid);
    $cnpUID = '14059359-D8E8-41C3-B628-E7E030537905';
    $cnpKey = '5DC1B75A-7EFA-4C01-BDCD-E02C536313A3';
    $connect1 = ['soap_version' => 'SOAP_1_1', 'trace' => 1, 'exceptions' => 0];
    $client1 = new \SoapClient('https://resources.connect.clickandpledge.com/wordpress/Auth2.wsdl', $connect1);
    if (isset($cnpacountid) && $cnpacountid !== '' && isset($cnpaccountGUID) && $cnpaccountGUID !== '') {
      $xmlr1 = new SimpleXMLElement('<GetAccountDetail></GetAccountDetail>');
      $xmlr1->addChild('accountId', $cnpacountid);
      $xmlr1->addChild('accountGUID', $cnpaccountGUID);
      $xmlr1->addChild('username', $cnpUID);
      $xmlr1->addChild('password', $cnpKey);
      $response1 = $client1->GetAccountDetail($xmlr1);
      return $response1;
    }
  }

  /**
   * To get account GUID assigned to an account.
   *
   * @param mixed $accid
   *   The account id.
   *
   * @return string
   *   Returns account GUID
   */
  public function getwcCnpAccountGuid($accid) {
    $cnpAccountGUId = '';
    $query = $this->connection->query('SELECT cnpaccountsinfo_accountguid FROM {dp_cnp_dp_jbcnpaccountsinfo} where cnpaccountsinfo_orgid =:accid', [':accid' => $accid]);
    $result = $query->fetchAssoc();
    $cnpAccountGUId = $result['cnpaccountsinfo_accountguid'];
    return $cnpAccountGUId;
  }

  /**
   * The AJAX call back to get the default payment options.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the parent form.
   *
   * @return array
   *   Returns The form structure.
   */
  public function checkDefaultPayments(array &$form, FormStateInterface $form_state) {
    return $form['cnp_default_payment'];
  }

  /**
   * The AJAX callback to get the default payment options.
   *
   * @param mixed $form_state
   *   The current state of the form.
   * @param mixed $config
   *   The configuration Object.
   *
   * @return array
   *   Returns an array with default payment options.
   */
  public function getPayOptions($form_state, $config) {
    $opt = [];
    if ($form_state->getValue("cnp_payment_methdos1")) {
      $data = $form_state->getValue("cnp_payment_methdos1");
      if ($data['CustomPayment']) {
        if ($form_state->getValue("cnp_customPayment_titles")) {
          $opt['Credit Card'] = $this->t('Credit Card');
          $opt['eCheck'] = $this->t('eCheck');
          $pt = $form_state->getValue('cnp_customPayment_titles');
          $arr = explode(';', $pt, -1);
          foreach ($arr as $a) {
            $opt[$a] = $a;
          }
        }
        else {
          $opt['Credit Card'] = $this->t('Credit Card');
          $opt['eCheck'] = $this->t('eCheck');
          $svaed_pay_methods = $config->get('cnp.cnp_customPayment_titles');
          $arr = explode(';', $svaed_pay_methods, -1);
          foreach ($arr as $a) {
            $opt[$a] = $a;
          }
        }
      }
      else {
        $opt['Credit Card'] = $this->t('Credit Card');
        $opt['eCheck'] = $this->t('eCheck');
      }
    }
    else {
      if ($config->get('cnp.cnp_payment_methdos1')) {
        $opt['Credit Card'] = $this->t('Credit Card');
        $opt['eCheck'] = $this->t('eCheck');
        $svaed_pay_methods = $config->get('cnp.cnp_customPayment_titles');
        $arr = explode(';', $svaed_pay_methods, -1);
        foreach ($arr as $a) {
          $opt[$a] = $a;
        }
      }
      else {
        $opt['Credit Card'] = $this->t('Credit Card');
        $opt['eCheck'] = $this->t('eCheck');
        $opt['COD'] = $this->t('COD');
      }
    }
    return $opt;
  }

  /**
   * To Update the default payments.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Returns the form structure.
   */
  public function updateDefaultPayments(array &$form, FormStateInterface $form_state) {
    return $form['cnp_default_payment'];
  }

}
